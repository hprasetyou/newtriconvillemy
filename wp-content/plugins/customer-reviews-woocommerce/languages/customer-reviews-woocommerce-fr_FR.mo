��          �      �       H  G   I     �     �     �     �     �  Z   �  b        r     u     �     �     �  `  �  Y   %  	     
   �  
   �  
   �  
   �  t   �  o   *     �     �     �  ,   �     �        	                          
                               %d out of %d people found this helpful. Was this review helpful to you? 1 star 2 star 3 star 4 star 5 star An error occurred with submission of your feedback. Please refresh the page and try again. An error occurred with submission of your feedback. Please report it to the website administrator. No Processing... Thank you for your feedback! Was this review helpful to you? Yes PO-Revision-Date: 2018-01-19 00:26+0000
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n > 1;
X-Generator: Poedit 2.0.5
Language: fr
Project-Id-Version: Plugins - Customer Reviews for WooCommerce - Stable (latest release)
POT-Creation-Date: 
Last-Translator: 
Language-Team: 
 %d personnes sur %d ont trouvé cette critique utile. Est-ce qu’elle vous a renseigné? 1 étoile 2 étoiles 3 étoiles 4 étoiles 5 étoiles Une erreur s’est produite pendant l’envoi de votre retour. Veuillez réactualiser la page et essayer à nouveau. Une erreur s’est produite pendant l’envoi de votre retour. Veuillez en notifier l’administrateur du site. Non Traitement en cours… Merci pour votre retour! Est-ce que cette critique vous a renseigné? Oui 