<?php
/*
Plugin Name: Schon Shortcodes
Plugin URI: http://themeforest.net/user/webbaku/portfolio
Description: Shortcodes and Slider for Schön theme
Version: 1.1.1
Author: Webbaku
Author URI: http://themeforest.net/user/webbaku/portfolio
*/

define( 'SCHON_SHORTCODES_PATH', plugin_dir_path( __FILE__ ) );
define( 'SCHON_SHORTCODES_URI', plugin_dir_url( __FILE__ ) );


load_plugin_textdomain('schon_shortcodes');

///**
//* Enqueue scripts and styles.
//*/
//function tructor_shortcodes_scripts() {
//wp_register_script( 'parallax-script', WBBK_SHORT_URI . 'inc/stellar/jquery.stellar.min.js', array('jquery'), '1.3.1', false );
//}
//add_action( 'wp_enqueue_scripts', 'tructor_shortcodes_scripts', 99);

require_once SCHON_SHORTCODES_PATH.'/shortcodes.php';
