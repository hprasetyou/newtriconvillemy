<?php
/**
 * The template for displaying all single posts.
 *
 * @package schon
 */

get_header(); ?>

	<div class="container">
		<div class="row">
			
			<div id="primary" class="content-area <?php echo schon_get_primary_class('sidebar-blog') ?>">
				<main id="main" class="site-main" role="main">

					<?php while ( have_posts() ) : the_post(); ?>

						<?php get_template_part( 'content', get_post_format() ); ?>


						<?php schon_render_author_box(); ?>

                        <?php
						if(schon_get_option('blog-show-post-navigation')) {
							the_post_navigation(array(
								'screen_reader_text' => esc_html__('Other Articles', 'schon'),
								'prev_text' => '<i class="fa fa-long-arrow-left" aria-hidden="true"></i> %title',
								'next_text' => '%title <i class="fa fa-long-arrow-right" aria-hidden="true"></i>'
							));
						}

						?>

						<?php
							// If comments are open or we have at least one comment, load up the comment template
							if ( comments_open() || get_comments_number() ) :
								comments_template();
							endif;
						?>

					<?php endwhile; // end of the loop. ?>

				</main><!-- #main -->
			</div><!-- #primary -->
			

			<?php schon_show_sidebar('sidebar-blog'); ?>

		</div><!-- .row -->
	</div><!-- .container -->

<?php get_footer(); ?>
