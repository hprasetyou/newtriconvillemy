<form role="search" method="get" id="searchform" action="<?php echo esc_url( home_url( '/' ) ); ?>">
	<div>
		<label class="screen-reader-text" for="s">Cerca:</label>
        <div class="width-200">
            <input type="text" value="" placeholder="<?php esc_html_e('Search', 'schon')?>" name="s" id="s" class="search-field form-control"/>
            <input type="submit" id="searchsubmit" value="<?php esc_html_e('Search', 'schon')?>" class="search-submit btn btn-default"/>
        </div>
	</div>
</form>