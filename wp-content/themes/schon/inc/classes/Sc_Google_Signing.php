<?php


if ( ! defined( 'ABSPATH' ) ) die( 'Direct access forbidden.' );

//use Google_Client;
//use Google_Oauth2Service;
//use Google_AuthException;

if( ! class_exists( 'Sc_Google_Signing' ) ) {
    /**
     * Class Sc_Google_Signing
     * Signing Up and Signing In with Google in
     *
     * @since 1.3.0
     */
    class Sc_Google_Signing
    {

        /**
         * Google access token from the API
         *
         * @var string
         */
        private $access_token = '';

        /**
         * Google user details Array
         *
         * @var array
         */
        private $google_details;

	    /**
	     * @var bool
	     */
	    private static $login_enabled;
	    
	    /**
	     * @var string
	     */
	    private static $google_app_id;

	    /**
	     * @var string
	     */
	    private static $google_app_secret;

        /**
         * Constructor
         */
        public function __construct()
        {

	        self::$login_enabled = schon_get_option('google_login_enabled', false);

	        if( !self::$login_enabled )
		        return;
	        
	        self::$google_app_id = schon_get_option('google_app_id');
	        self::$google_app_secret = schon_get_option('google_app_secret');
	        
            // Ajax Registration
            add_action( 'wp_ajax_schon_google_login', array($this, 'apiCallback'));
            add_action( 'wp_ajax_nopriv_schon_google_login', array($this, 'apiCallback'));

        }

        /**
         * Check whether the sign up / in with Google is allowed
         *
         * @return bool
         */
        static function isEnabled() {

            return ( self::$login_enabled && self::$google_app_id && self::$google_app_secret );

        }

        /**
         * Returns the site Google API Callback URL
         *
         * @return string
         */
        static function getCallbackUrl() {

            return admin_url( 'admin-ajax.php?action=schon_google_login' );

        }

        /**
         * Login URL to API
         *
         * @return string
         */
        static function getLoginUrl() {

//            if(!session_id()) {
//                session_start();
//            }

            $gClient = self::initApi();

            $url = $gClient->createAuthUrl();

            return esc_url($url);

        }

        /**
         * Init the Api Connection
         *
         * @return Google_Client
         */
        static function initApi() {

            $gClient = new Google_Client();
            $gClient->setApplicationName(__("Schon Google Login","schon"));
            $gClient->setClientId( self::$google_app_id );
            $gClient->setClientSecret( self::$google_app_secret );
            $gClient->setRedirectUri(self::getCallbackUrl());
            $gClient->setScopes(array('https://www.googleapis.com/auth/plus.me','https://www.googleapis.com/auth/userinfo.email'));

            return $gClient;

        }

        /**
         * Callback for the API call
         */
        public function apiCallback() {

            if(!self::isEnabled())
                wp_die();

            if(!session_id()) {
                session_start();
            }

            // Init the api
            $gClient = self::initApi();

            // We use the OauthV2 service
            $google_oauthV2 = new Google_Oauth2Service($gClient);

            // Get a private token
            try {
                if(isset($_GET['code'])){
                    $gClient->authenticate($_GET['code']);
                }
                $this->access_token = $gClient->getAccessToken();
            }
            // If that fails, we throw our error
            catch (Google_AuthException $e) {
                //Eo_Alert::create()->setType('error')->setContent($e->getMessage())->queue();
	            
	            throw new Exception( $e );
            }

            // If we got no token
            if (empty($this->access_token)) {
	            wp_redirect( get_permalink( get_option( 'woocommerce_myaccount_page_id' ) ) );
                die();
            }

            // Fetch the user information
            $this->google_details = $google_oauthV2->userinfo->get();

	        $user = $this->fetchUser( $this->google_details );

	        // If the user exists, log in him, otherwise create it
	        if( $user instanceof \WP_User) {
		        $this->loginUser( $user );
	        } else {
		        $this->createUser();
	        }

	        wp_redirect( get_permalink( get_option( 'woocommerce_myaccount_page_id' ) ) );
            die();

        }

	    /**
	     * Login an user to WordPress
	     *
	     * @param \WP_User $user
	     *
	     * @return bool|void
	     */
        private function loginUser( \WP_User $user ) {

	        // Log the user
	        wp_set_auth_cookie( $user->ID );

            // We add an  alert
            //Eo_Alert::create()->setType('success')->setContent(__('Welcome back', 'schon') .' '. eo_get_display_name($user) .'!')->queue();

        }

        /**
         * Create a new WordPress account using Google Details and redirect once done
         */
        private function createUser() {


            $user = $this->google_details;

            // Create an username
            $username = sanitize_user(str_replace(' ', '_', strtolower($user['name'])));
			$email = sanitize_email($user['email']);

	        // If there isn't any name, use the alias of the email
	        if( empty($username) ) {
		        $email_exploded = explode( '@', $email );
		        $username = $email_exploded[0];
	        }

            // Creating our user
            $new_user_id = wp_create_user($username, wp_generate_password(), $user['email']);

            if(!is_int($new_user_id)) {
                return;
            }

            // Setting the meta
            if( isset($user['first_name']) && !empty($user['first_name']))
	            update_user_meta( $new_user_id, 'first_name', $user['first_name'] );

	        if( isset($user['last_name']) && !empty($user['last_name']))
                update_user_meta( $new_user_id, 'last_name', $user['last_name'] );

	        if( isset($user['user_url']) && !empty($user['user_url']))
                update_user_meta( $new_user_id, 'user_url', $user['link'] );

            update_user_meta( $new_user_id, 'user_email', $email );
	        update_user_meta( $new_user_id, 'schon_google_id', $user['id'] );

            /**
             * Action `schon_google_after_signup`
             *
             * @param $new_user_id int
             */
            do_action('schon_google_after_signup', $new_user_id);

            // Log the user?
            wp_set_auth_cookie( $new_user_id );
            // We add an  alert
            //Eo_Alert::create()->setType('success')->setContent(__('Welcome', 'schon') .' '. eo_get_display_name($new_user_id) .'!')->queue();

        }

	    /**
	     * Using the google details, get the corresponding user into the db
	     *
	     * @param array $google_details
	     *
	     * @return false|\WP_User
	     */
	    private function fetchUser( $google_details) {

		    // We look for the `schon_google_id` to see if there is any match
		    $wp_users = get_users(array(
			    'meta_key'     => 'schon_google_id',
			    'meta_value'   => $google_details['id'],
			    'number'       => 1,
			    'count_total'  => false,
		    ));

		    if(empty($wp_users[0])) {
			    $wp_user = get_user_by( 'email', $google_details['email'] );
		    } else
			    $wp_user = $wp_users[0];

		    // If user exists, update the google id, for the future fetches
		    if( $wp_user instanceof \WP_User ) {
			    add_user_meta( $wp_user->ID, 'schon_google_id', $google_details['id'] );
		        return $wp_user;
		    }

		    //if user doesn't exists
		    return false;
	    }
	    
    }
}

/**
 * Let's fire it :
 */
new Sc_Google_Signing();