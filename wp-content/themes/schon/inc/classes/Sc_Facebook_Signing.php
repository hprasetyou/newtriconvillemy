<?php

use Facebook\Facebook;
use Facebook\Exceptions\FacebookSDKException;
use Facebook\Exceptions\FacebookResponseException;

if ( ! defined( 'ABSPATH' ) ) die( 'Direct access forbidden.' );

if( ! class_exists( 'Sc_Facebook_Signing' ) ) {
    /**
     * Class Sc_Facebook_Signing
     * Signing Up and Signing In with Facebook
     *
     * @since 1.3.0

     */
    class Sc_Facebook_Signing
    {

        /**
         * Facebook access token from the API
         *
         * @var string
         */
        private $access_token = '';

        /**
         * Facebook user details Array
         *
         * @var array
         */
        private $facebook_details;

	    /**
	     * @var bool
	     */
	    private static $facebook_login_enabled;

	    /**
	     * @var string
	     */
	    private static $facebook_app_id;

	    /**
	     * @var string
	     */
	    private static $facebook_app_secret;

        /**
         * Constructor
         */
        public function __construct()
        {

	        self::$facebook_login_enabled = schon_get_option('facebook_login_enabled', false);

	        if( !self::$facebook_login_enabled )
		        return;

	        self::$facebook_app_id = schon_get_option('facebook_app_id');
	        self::$facebook_app_secret = schon_get_option('facebook_app_secret');


            // Ajax Registration
            add_action( 'wp_ajax_schon_facebook_login', array($this, 'apiCallback'));
            add_action( 'wp_ajax_nopriv_schon_facebook_login', array($this, 'apiCallback'));

        }

        /**
         * Check whether the sign up / in with Facebook is allowed
         *
         * @return bool
         */
        static function isEnabled() {

            if(self::$facebook_login_enabled && !empty(self::$facebook_app_id) && !empty(self::$facebook_app_secret))
                return true;

            return false;

        }

        /**
         * Returns the site Facebook API Callback URL
         *
         * @return string
         */
        static function getCallbackUrl() {

            return admin_url( 'admin-ajax.php?action=schon_facebook_login' );

        }

        /**
         * Login URL to Facebook API
         *
         * @return string
         */
        static function getLoginUrl() {

//	        var_dump(session_id());
//            if(!session_id()) {
//                session_start();
//            }

            $fb = self::initApi();

            $helper = $fb->getRedirectLoginHelper();

            // Optional permissions
            $permissions = ['email'];

            $url = $helper->getLoginUrl(self::getCallbackUrl(), $permissions);

            return esc_url($url);

        }

        /**
         * Init the Api Connection
         *
         * @return Facebook
         */
        static function initApi() {

            $facebook = new Facebook([
                'app_id' => self::$facebook_app_id,
                'app_secret' => self::$facebook_app_secret,
                'default_graph_version' => 'v2.2',
                'persistent_data_handler' => 'session'
            ]);


            return $facebook;

        }

        /**
         * Get user details through the Facebook API
         *
         * @link https://developers.facebook.com/docs/facebook-login/permissions#reference-public_profile
         * @param $fb Facebook
         * @return \Facebook\GraphNodes\GraphUser
         */
        private function getUserDetails($fb)
        {

            try {
                $response = $fb->get('/me?fields=id,name,first_name,last_name,email,link', $this->access_token);
            } catch(FacebookResponseException $e) {
                //$message = esc_html__('Graph returned an error: ','schon'). $e->getMessage();
                //Eo_Alert::create()->setType('error')->setContent($message)->queue();
	            throw new Exception( $e );

            } catch(FacebookSDKException $e) {
                //$message = esc_html__('Facebook SDK returned an error: ','schon'). $e->getMessage();
                //Eo_Alert::create()->setType('error')->setContent($message)->queue();
	            throw new Exception( $e );

            }

            return $response->getGraphUser();

        }

        /**
         * Callback for the facebook API call
         */
        public function apiCallback() {


            if(!self::isEnabled())
                wp_die();

            if(!session_id()) {
                session_start();
            }

            // We start the connection
            $fb = self::initApi();

            // Load the helper
            $helper = $fb->getRedirectLoginHelper();
            $_SESSION['FBRLH_state'] = $_GET['state'];


            // Try to get access
            try {
                $accessToken = $helper->getAccessToken();
            }
            // When Graph returns an error
            catch(FacebookResponseException $e) {
                //$message = esc_html__('Graph returned an error: ','schon'). $e->getMessage();
                //Eo_Alert::create()->setType('error')->setContent($message)->queue();
	            throw new Exception( $e );

            }
            // When validation fails or other local issues
            catch(FacebookSDKException $e) {
                //$message = esc_html__('Facebook SDK returned an error: ','schon'). $e->getMessage();
                //Eo_Alert::create()->setType('error')->setContent($message)->queue();
	            throw new Exception( $e );
	            
            }

            // If we got nothing
            if (!isset($accessToken)) {
	            wp_redirect( get_permalink( get_option( 'woocommerce_myaccount_page_id' ) ) );
                die();
            }

            // We save the token in our instance
            $this->access_token = $accessToken->getValue();

            $this->facebook_details = $this->getUserDetails($fb);

	        //$avatar = $this->getUserAvatar($fb);

	        $user = $this->fetchUser( $this->facebook_details );

	        // If the user exists, log in him, otherwise create it
	        if( $user instanceof \WP_User) {
		        $this->loginUser( $user );
	        } else {
		        $this->createUser();
	        }

            wp_redirect( get_permalink( get_option( 'woocommerce_myaccount_page_id' ) ) );
            die();

        }

	    /**
	     * Login an user to WordPress
	     *
	     * @param \WP_User $user
	     *
	     * @return bool|void
	     */
	    private function loginUser( \WP_User $user ) {

            // Log the user
            wp_set_auth_cookie( $user->ID );

            // We add an  alert
           // Eo_Alert::create()->setType('success')->setContent(__('Welcome back', 'schon') .' '. eo_get_display_name($user->ID) .'!')->queue();

        }

        /**
         * Create a new WordPress account using Facebook Details and redirect once done
         */
        private function createUser() {


            $fb_user = $this->facebook_details;

            // Create an username
            $username = sanitize_user(str_replace(' ', '_', strtolower($this->facebook_details['name'])));
	        $email = sanitize_email($fb_user['email']);

	        // If there isn't any name, use the alias of the email
	        if( empty($username) ) {
		        $email_exploded = explode( '@', $email );
		        $username = $email_exploded[0];
	        }

            // Creating our user
            $new_user_id = wp_create_user($username, wp_generate_password(), $email);

            if(!is_int($new_user_id)) {
                return;
            }

            // Setting the meta
            update_user_meta( $new_user_id, 'first_name', $fb_user['first_name'] );
            update_user_meta( $new_user_id, 'last_name', $fb_user['last_name'] );
            update_user_meta( $new_user_id, 'user_url', $fb_user['link'] );
            update_user_meta( $new_user_id, 'schon_facebook_id', $fb_user['id'] );

            /**
             * Action `schon_facebook_after_signup`
             *
             * @param $new_user_id int
             */
            do_action('schon_facebook_after_signup', $new_user_id);

            // Log the user?
            wp_set_auth_cookie( $new_user_id );
            // We add an  alert
            //Eo_Alert::create()->setType('success')->setContent(__('Welcome', 'schon') .' '. eo_get_display_name($new_user_id) .'!')->queue();

        }

	    /**
	     * Using the facebook details, get the corresponding user into the db
	     *
	     * @param array $facebook_details
	     *
	     * @return false|\WP_User
	     */
	    private function fetchUser( $facebook_details ) {

		    // We look for the `schon_facebook_id` to see if there is any match
		    $wp_users = get_users(array(
			    'meta_key'     => 'schon_facebook_id',
			    'meta_value'   => $facebook_details['id'],
			    'number'       => 1,
			    'count_total'  => false,
		    ));

		    if(empty($wp_users[0])) {
			    $wp_user = get_user_by( 'email', $facebook_details['email'] );
		    } else
			    $wp_user = $wp_users[0];

		    // If user exists, add the facebook id, for the future fetches
		    if( $wp_user instanceof \WP_User ) {
			    add_user_meta( $wp_user->ID, 'schon_facebook_id', $facebook_details['id'] );
			    return $wp_user;
		    }

		    //if user doesn't exists
		    return false;
	    }

    }
}

/**
 * Let's fire it :
 */
new Sc_Facebook_Signing();