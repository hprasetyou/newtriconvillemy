<?php

if(!session_id()) {
    session_start();
}

$load_facebook_files = apply_filters( 'schon_load_facebook_login_files', true );
if($load_facebook_files) {
	require_once get_template_directory() . '/inc/libs/facebook-sdk/autoload.php';
	require_once get_template_directory() . '/inc/classes/Sc_Facebook_Signing.php';
}
$load_google_files = apply_filters( 'schon_load_google_login_files', true );
if($load_google_files) {
	require_once get_template_directory() . '/inc/libs/google-sdk/Google_Client.php';
	require_once get_template_directory() . '/inc/libs/google-sdk/contrib/Google_Oauth2Service.php';
	require_once get_template_directory() . '/inc/classes/Sc_Google_Signing.php';
}


function schon_render_login_popup() {

	if( !schon_login_popup_allowed() )
		return;

	$social_enabled = (Sc_Facebook_Signing::isEnabled() || Sc_Google_Signing::isEnabled());
	$social_enabled_class = ($social_enabled) ? 'social-enabled' : '';

	echo '<div id="login-popup-overlay"></div>';
	echo '<div id="login-popup" class="woocommerce ' . $social_enabled_class . '">';

		echo '<a href="javascript:void(0)" id="login-popup-close-trigger"><i class="fa fa-times"></i> ' . esc_html__('Close', 'schon') . '</a>';

		echo '<h2>' . esc_html_x('Sign In', 'Thea heading of the login popup', 'schon') . '</h2>';

		if( $social_enabled ) {

			echo '<div class="login-popup__social-buttons">';
				if( Sc_Facebook_Signing::isEnabled() ) {
					echo '<a href="' . Sc_Facebook_Signing::getLoginUrl() . '" class="btn btn-default btn-facebook"><i class="fa fa-facebook"></i> ' . esc_html__( 'Log in with Facebook', 'schon' ) .'</a>';
				}

				if( Sc_Google_Signing::isEnabled() ) {
					echo '<a href="' . Sc_Google_Signing::getLoginUrl() . '" class="btn btn-default btn-google"><i class="fa fa-google-plus"></i> ' . esc_html__( 'Log in with Google', 'schon' ) .'</a>';
				}
			echo '</div>';

		}

		echo '<div class="login-popup__form-wrap">';
			woocommerce_login_form();

			if( get_option( 'woocommerce_enable_myaccount_registration' ) === 'yes' )
				echo '<a href="'.get_permalink( get_option('woocommerce_myaccount_page_id') ).'">' . esc_html__('Sign Up', 'schon') . '</a>';
		echo '</div>';

	echo '</div>';

}
add_action( 'wp_footer', 'schon_render_login_popup');

if ( !function_exists('schon_render_social_buttons') ) {
	/**
	 * Render the social login buttons on My Account Page
	 */
	function schon_render_social_buttons() {

		if( (function_exists('is_account_page')) && !is_account_page() || !Sc_Facebook_Signing::isEnabled() && !Sc_Google_Signing::isEnabled() )
			return;

		echo '<div class="my_account_login-social-buttons">';
			if( Sc_Facebook_Signing::isEnabled() ) {
				echo '<a href="' . Sc_Facebook_Signing::getLoginUrl() . '" class="btn btn-default btn-facebook"><i class="fa fa-facebook"></i> ' . esc_html__( 'Log in with Facebook', 'schon' ) .'</a>';
			}

			if( Sc_Google_Signing::isEnabled() ) {
				echo '<a href="' . Sc_Google_Signing::getLoginUrl() . '" class="btn btn-default btn-google"><i class="fa fa-google-plus"></i> ' . esc_html__( 'Log in with Google', 'schon' ) .'</a>';
			}
		echo '</div>';

	}
}
add_action( 'woocommerce_login_form_start', 'schon_render_social_buttons');

if ( !function_exists( 'schon_process_login' ) ) {
	/**
	 * This function override the methid WC_Form_Handler::process_login
	 *
	 * If a login is incorrect, it redirect to the page "My account"
	 */
	function schon_process_login() {
		$nonce_value = isset( $_POST['_wpnonce'] ) ? $_POST['_wpnonce'] : '';
		$nonce_value = isset( $_POST['woocommerce-login-nonce'] ) ? $_POST['woocommerce-login-nonce'] : $nonce_value;

		if ( ! empty( $_POST['login'] ) && wp_verify_nonce( $nonce_value, 'woocommerce-login' ) ) {

			try {
				$creds = array(
					'user_password' => $_POST['password'],
					'remember'      => isset( $_POST['rememberme'] ),
				);

				$username         = trim( $_POST['username'] );
				$validation_error = new WP_Error();
				$validation_error = apply_filters( 'woocommerce_process_login_errors', $validation_error, $_POST['username'], $_POST['password'] );

				if ( $validation_error->get_error_code() ) {
					throw new Exception( '<strong>' . __( 'Error:', 'woocommerce' ) . '</strong> ' . $validation_error->get_error_message() );
				}

				if ( empty( $username ) ) {
					throw new Exception( '<strong>' . __( 'Error:', 'woocommerce' ) . '</strong> ' . __( 'Username is required.', 'woocommerce' ) );
				}

				if ( is_email( $username ) && apply_filters( 'woocommerce_get_username_from_email', true ) ) {
					$user = get_user_by( 'email', $username );

					if ( isset( $user->user_login ) ) {
						$creds['user_login'] = $user->user_login;
					} else {
						throw new Exception( '<strong>' . __( 'Error:', 'woocommerce' ) . '</strong> ' . __( 'A user could not be found with this email address.', 'woocommerce' ) );
					}
				} else {
					$creds['user_login'] = $username;
				}

				// On multisite, ensure user exists on current site, if not add them before allowing login.
				if ( is_multisite() ) {
					$user_data = get_user_by( 'login', $username );

					if ( $user_data && ! is_user_member_of_blog( $user_data->ID, get_current_blog_id() ) ) {
						add_user_to_blog( get_current_blog_id(), $user_data->ID, 'customer' );
					}
				}

				// Perform the login
				$user = wp_signon( apply_filters( 'woocommerce_login_credentials', $creds ), is_ssl() );

				if ( is_wp_error( $user ) ) {
					$message = $user->get_error_message();
					$message = str_replace( '<strong>' . esc_html( $creds['user_login'] ) . '</strong>', '<strong>' . esc_html( $username ) . '</strong>', $message );
					throw new Exception( $message );
				} else {

					if ( ! empty( $_POST['redirect'] ) ) {
						$redirect = $_POST['redirect'];
					} elseif ( wp_get_referer() ) {
						$redirect = wp_get_referer();
					} else {
						$redirect = wc_get_page_permalink( 'myaccount' );
					}

					wp_redirect( apply_filters( 'woocommerce_login_redirect', $redirect, $user ) );
					exit;
				}
			} catch ( Exception $e ) {
				wc_add_notice( apply_filters( 'login_errors', $e->getMessage() ), 'error' );
				if ( ! WC()->session->has_session() ) {
					WC()->session->set_customer_session_cookie( true );

				}

				do_action( 'woocommerce_login_failed' );
				wp_redirect( get_permalink( get_option( 'woocommerce_myaccount_page_id' ) ) );
			}
		}


	}
}
remove_action( 'wp_loaded', array( WC_Form_Handler::class, 'process_login' ), 20 );
add_action( 'wp_loaded', 'schon_process_login', 20 );

if( !function_exists( 'schon_login_popup_allowed' ) ) {
	/**
	 * Check if the login popup is allowed on the current page
	 *
	 * @return bool
	 */
	function schon_login_popup_allowed() {
		return (
			! is_user_logged_in()
			&& ! ( function_exists( 'is_account_page' ) && is_account_page() )
			&& apply_filters( 'schon_login_popup_allowed', true )
		);
	}
}
/* LOGIN/REGISTER POPUP */
