<?php
/**
 * Custom functions that act independently of the theme templates
 *
 * Eventually, some of the functionality here could be replaced by core features
 *
 * @package schon
 */



/**
 * Adds custom classes to the array of body classes.
 *
 * @param array $classes Classes for the body element.
 * @return array
 */
function schon_body_classes( $classes ) {
	// Adds a class of group-blog to blogs with more than 1 published author.
	if ( is_multi_author() ) {
		$classes[] = 'group-blog';
	}

    // Adds a class of hfeed to non-singular pages.
    if ( ! is_singular() ) {
        $classes[] = 'hfeed';
    }

	if(is_woocommerce())
		$classes[] = 'columns-'.schon_get_option('woocommerce-number-of-columns', 3);

	$sidebar_option = schon_get_sidebar_option();
	$classes[] = 'schon-sidebar-layout-' . $sidebar_option;

	return $classes;
}
add_filter( 'body_class', 'schon_body_classes' );

//schon_wp_title
function wp_editor_fontsize_filter( $buttons ) {
	array_shift( $buttons );
	
	array_unshift( $buttons, 'fontsizeselect');
	array_unshift( $buttons, 'formatselect');
	

	return $buttons;
}
add_filter('mce_buttons_2', 'wp_editor_fontsize_filter');

/**
 * Printe into a <pre> tag
 * @param $value
 */
function schon_print($value) {
	echo '<div class="eonet_print_r"><pre>';
	var_dump($value);
	echo '</pre></div>';
}