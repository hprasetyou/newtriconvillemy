<?php
function schon_integrateWithVC() {
	vc_icon_element_fonts_enqueue('fontawesome');

	//[schon_banner]
	vc_map(array(
		"name" => esc_html__("Banner", 'schon'),
		"base" => "schon_banner",
		"category" => 'schon',
		"params" => array(
			array(
				"type" => "dropdown",
				"class" => "",
				"heading" => esc_html__("Height", 'schon'),
				"param_name" => "height",
				'value' => array(
//					esc_html__('12/12', 'schon') => 'height_layout_12-12',
//					esc_html__('9/12', 'schon') => 'height_layout_9-12',
//					esc_html__('8/12', 'schon') => 'height_layout_8-12',
//					esc_html__('6/12', 'schon') => 'height_layout_6-12',
//					esc_html__('4/12', 'schon') => 'height_layout_4-12',
//					esc_html__('3/12', 'schon') => 'height_layout_3-12',

					esc_html__('1/1', 'schon') => 'height_layout_1-1',
					esc_html__('1/2', 'schon') => 'height_layout_1-2',
					esc_html__('2/3', 'schon') => 'height_layout_2-3',
					esc_html__('1/3', 'schon') => 'height_layout_1-3',
				),
			),

			array(
				'type' => 'colorpicker',
				'heading' => esc_html__( 'Background color', 'schon' ),
				'param_name' => 'background_color',
				'value' => '#f0f0f0',
			),

			array(
				"type" => "attach_image",
				"class" => "",
				"heading" => esc_html__("Background image", 'schon'),
				"param_name" => "background_image",
			),

			array(
				"type" => "dropdown",
				"class" => "",
				"heading" => esc_html__("Background image style", 'schon'),
				"param_name" => "background_image_style",
				'value' => array(
					esc_html__('Theme Default', 'schon') => '',
					esc_html__('Cover', 'schon') => 'cover',
					esc_html__('Contain', 'schon') => 'contain',
					esc_html__('No repeat', 'schon') => 'no-repeat',
					esc_html__('Repeat', 'schon') => 'repeat',
				),
			),

			array(
				'type' => 'textfield',
				'heading' => esc_html__( 'Extra class name', 'js_composer' ),
				'param_name' => 'el_class',
				'description' => esc_html__( 'Style particular content element differently - add a class name and refer to it in custom CSS.', 'js_composer' ),
			),

			array(
				"type" => "textarea_html",
				"holder" => "div",
				"class" => "",
				"heading" => esc_html__("Content", 'schon'),
				"param_name" => "content",
				"value" => '',
				"description" => esc_html__("Enter your content.", 'schon'),
				'group' => esc_html__('Content', 'schon'),
			),

			array(
				"type" => "dropdown",
				"class" => "",
				"heading" => esc_html__("Content Alignment", 'schon'),
				"param_name" => "content_alignment",
				'value' => array(
					esc_html__('Center', 'schon') => 'v-center h-center',
					esc_html__('Top', 'schon') => 'v-top h-center',
					esc_html__('Right', 'schon') => 'v-center h-right',
					esc_html__('Bottom', 'schon') => 'v-bottom h-center',
					esc_html__('Left', 'schon') => 'v-center h-left',
					esc_html__('Top - Right', 'schon') => 'v-top h-right',
					esc_html__('Bottom - Right', 'schon') => 'v-bottom h-right',
					esc_html__('Bottom - Left', 'schon') => 'v-bottom h-left',
					esc_html__('Top - Left', 'schon') => 'v-top h-left'
				),
				'std' => 'v-top h-left',
				'group' => esc_html__('Content', 'schon'),
			),
			array(
				"type" => "dropdown",
				"class" => "",
				"heading" => esc_html__("Content width", 'schon'),
				"param_name" => "content_width",
				'value' => array(
					esc_html__('25%', 'schon') => 'width-25',
					esc_html__('33%', 'schon') => 'width-33',
					esc_html__('50%', 'schon') => 'width-50',
					esc_html__('66%', 'schon') => 'width-66',
					esc_html__('75%', 'schon') => 'width-75',
					esc_html__('100%', 'schon') => 'width-100',
				),
				'std' => 'width-75',
				'group' => esc_html__('Content', 'schon'),
			),
			array(
				'type' => 'dropdown',
				'heading' => esc_html__( 'Force color:', 'schon' ),
				'param_name' => 'content_force_color',
				'value' => array(
					esc_html__('None', 'schon') => '',
					esc_html__('White', 'schon') => 'text-force-white',
					esc_html__('Black', 'schon') => 'text-force-black',
				),
				'group' => esc_html__('Content', 'schon'),
			),
//			array(
//				"type" => "dropdown",
//				"class" => "",
//				"heading" => esc_html__("Content horizontal alignment", 'schon'),
//				"param_name" => "content_horizontal_alignment",
//				'value' => array(
//					esc_html__('Left', 'schon') => 'h-left',
//					esc_html__('Center', 'schon') => ' h-center',
//					esc_html__('Right', 'schon') => 'h-right',
//				),
//			),
//			array(
//				"type" => "dropdown",
//				"class" => "",
//				"heading" => esc_html__("Content vertical alignment", 'schon'),
//				"param_name" => "content_vertical_alignment",
//				'value' => array(
//					esc_html__('Top', 'schon') => 'v-top',
//					esc_html__('Center', 'schon') => 'v-center',
//					esc_html__('Bottom', 'schon') => 'v-bottom',
//				)
//			),
			//vc_map_add_css_animation(true),

			array(
				"type" => "attach_image",
				"class" => "",
				"heading" => esc_html__("Image", 'schon'),
				"param_name" => "attached_image",
				'group' => esc_html__('Attached image', 'schon'),
			),
			array(
				"type" => "dropdown",
				"class" => "",
				"heading" => esc_html__("Image alignment", 'schon'),
				"param_name" => "image_alignment",
				'group' => esc_html__('Attached image', 'schon'),
				'value' => array(
					esc_html__('Center', 'schon') => 'v-center h-center',
					esc_html__('Top', 'schon') => 'v-top h-center',
					esc_html__('Right', 'schon') => 'v-center h-right',
					esc_html__('Bottom', 'schon') => 'v-bottom h-center',
					esc_html__('Left', 'schon') => 'v-center h-left',
					esc_html__('Top - Right', 'schon') => 'v-top h-right',
					esc_html__('Bottom - Right', 'schon') => 'v-bottom h-right',
					esc_html__('Bottom - Left', 'schon') => 'v-bottom h-left',
					esc_html__('Top - Left', 'schon') => 'v-top h-left'
				),
				'std' => 'v-bottom h-right'
			),
			array(
				"type" => "dropdown",
				"class" => "",
				"heading" => esc_html__("Image width", 'schon'),
				"param_name" => "image_width",
				'group' => esc_html__('Attached image', 'schon'),
				'value' => array(
					esc_html__('25%', 'schon') => 'width-25',
					esc_html__('33%', 'schon') => 'width-33',
					esc_html__('50%', 'schon') => 'width-50',
					esc_html__('66%', 'schon') => 'width-66',
					esc_html__('75%', 'schon') => 'width-75',
					esc_html__('100%', 'schon') => 'width-100',
				),
				'std' => 'width-50'
			),
//			array(
//				'type' => 'textfield',
//				'heading' => esc_html__( 'Vertical offset', 'schon' ),
//				'param_name' => 'image_vertical_offset',
//				//Todo change this
//				'description' => esc_html__( 'Vertical offset of the image expressend in pixel (Please insert just the number without "px")', 'schon' ),
//				'group' => esc_html__('Attached image', 'schon'),
//			),
//			array(
//				'type' => 'textfield',
//				'heading' => esc_html__( 'Horizontal offset', 'schon' ),
//				'param_name' => 'image_horizontal_offset',
//				//Todo change this
//				'description' => esc_html__( 'Horizontal offset of the image expressend in pixel (Please insert just the number without "px")', 'schon' ),
//				'group' => esc_html__('Attached image', 'schon'),
//			),

			array(
				'type' => 'textfield',
				'heading' => esc_html__( 'Link of the banner', 'schon' ),
				'param_name' => 'link_to',
				'description' => esc_html__( 'The link of the banner (assigned to the whole banner by default)', 'schon' ),
				'group' => esc_html__('Link/Hover', 'schon'),
			),
			array(
				"type" => "checkbox",
				"heading" => esc_html__('Create button', 'schon'),
				"param_name" => "create_button",
				'description' => esc_html__( 'If this is checked, the whole banner will not be clickable and a button link will be created.', 'schon' ),
				'group' => esc_html__('Link/Hover', 'schon'),
				'value' => '',
			),
			array(
				"type" => "dropdown",
				"heading" => esc_html__("Hover style", 'schon'),
				"param_name" => "hover_style",
				'group' => esc_html__('Link/Hover', 'schon'),
				'value' => array(
					esc_html__('None', 'schon') => '',
					esc_html__('Dark', 'schon') => 'dark',
					esc_html__('Light', 'schon') => 'light',
				),
				'std' => 'dark',
				'dependency' => array(
					'element' => 'create_button',
					'value_not_equal_to' => array('true'),
				),
			),
			array(
				'type' => 'textfield',
				'heading' => esc_html__( 'Button Anchor', 'schon' ),
				'param_name' => 'button_anchor',
				'group' => esc_html__('Link/Hover', 'schon'),
				'dependency' => array(
					'element' => 'create_button',
					'value' => 'true',
				),
			),
			array(
				'type' => 'iconpicker',
				'heading' => __( 'Icon', 'schon' ),
				'param_name' => 'button_icon',
				'value' => 'fa fa-arrow-circle-o-right',
				'group' => esc_html__('Link/Hover', 'schon'),
				'settings' => array(
					'emptyIcon' => true,
					// default true, display an "EMPTY" icon?
					'type' => 'fontawesome',
				),
				'dependency' => array(
					'element' => 'create_button',
					'value' => 'true',
				),
			),
			array(
				"type" => "dropdown",
				"heading" => esc_html__("Button style", 'schon'),
				"param_name" => "button_style",
				'group' => esc_html__('Link/Hover', 'schon'),
				'value' => array(
					esc_html__('Just text', 'schon') => 'no-padding',
					esc_html__('Default', 'schon') => 'btn-default',
					esc_html__('Default ghost', 'schon') => 'btn-default btn-ghost',
					esc_html__('Primary', 'schon') => 'btn-primary',
					esc_html__('Primary ghost', 'schon') => 'btn-primary btn-ghost',
					esc_html__('Secondary', 'schon') => 'btn-primary-2',
					esc_html__('Secondary ghost', 'schon') => 'btn-primary-2 btn-ghost',
					esc_html__('Info', 'schon') => 'btn-info',
					esc_html__('Info ghost', 'schon') => 'btn-info btn-ghost',
					esc_html__('Success', 'schon') => 'btn-success',
					esc_html__('Success ghost', 'schon') => 'btn-success btn-ghost',
					esc_html__('Warning', 'schon') => 'btn-warning',
					esc_html__('Warning ghost', 'schon') => 'btn-warning btn-ghost',
					esc_html__('Danger', 'schon') => 'btn-danger',
					esc_html__('Danger ghost', 'schon') => 'btn-danger btn-ghost',
				),
				'dependency' => array(
					'element' => 'create_button',
					'value' => 'true',
				),
			),
			array(
				"type" => "dropdown",
				"heading" => esc_html__("Button size", 'schon'),
				"param_name" => "button_size",
				'group' => esc_html__('Link/Hover', 'schon'),
				'value' => array(
					esc_html__('Very small', 'schon') => 'btn-xs',
					esc_html__('Small', 'schon') => 'btn-sm',
					esc_html__('Medium', 'schon') => ' ',
					esc_html__('Large', 'schon') => 'btn-lg',
				),
				'std' => ' ',
				'dependency' => array(
					'element' => 'create_button',
					'value' => 'true',
				),
			),
			array(
				"type" => "dropdown",
				"heading" => esc_html__("Button alignment", 'schon'),
				"param_name" => "button_alignment",
				'group' => esc_html__('Link/Hover', 'schon'),
				'value' => array(
					esc_html__('Center', 'schon') => 'v-center h-center',
					esc_html__('Top', 'schon') => 'v-top h-center',
					esc_html__('Right', 'schon') => 'v-center h-right',
					esc_html__('Bottom', 'schon') => 'v-bottom h-center',
					esc_html__('Left', 'schon') => 'v-center h-left',
					esc_html__('Top - Right', 'schon') => 'v-top h-right',
					esc_html__('Bottom - Right', 'schon') => 'v-bottom h-right',
					esc_html__('Bottom - Left', 'schon') => 'v-bottom h-left',
					esc_html__('Top - Left', 'schon') => 'v-top h-left'
				),
				'std' => 'v-bottom h-left',
				'dependency' => array(
					'element' => 'create_button',
					'value' => 'true',
				),
			),

			array(
				'type' => 'colorpicker',
				'heading' => esc_html__( 'Force color:', 'schon' ),
				'description' => "It's applied to the button color or to the overlay color of the link", 
				'param_name' => 'link_force_color',
				'value' => '',
				'group' => esc_html__('Link/Hover', 'schon'),
			),

			array(
				'type' => 'textfield',
				'heading' => esc_html__( 'Badge text', 'schon' ),
				'param_name' => 'badge_text',
				'group' => esc_html__('Badge', 'schon'),
			),
			array(
				'type' => 'colorpicker',
				'heading' => esc_html__( 'Badge background', 'schon' ),
				'param_name' => 'badge_background',
				'value' => '#b2cc29',
				'group' => esc_html__('Badge', 'schon'),
			),
			array(
				"type" => "dropdown",
				"class" => "",
				"heading" => esc_html__("Badge alignment", 'schon'),
				"param_name" => "badge_alignment",
				'group' => esc_html__('Badge', 'schon'),
				'value' => array(
					esc_html__('Center', 'schon') => 'v-center h-center',
					esc_html__('Top', 'schon') => 'v-top h-center',
					esc_html__('Right', 'schon') => 'v-center h-right',
					esc_html__('Bottom', 'schon') => 'v-bottom h-center',
					esc_html__('Left', 'schon') => 'v-center h-left',
					esc_html__('Top - Right', 'schon') => 'v-top h-right',
					esc_html__('Bottom - Right', 'schon') => 'v-bottom h-right',
					esc_html__('Bottom - Left', 'schon') => 'v-bottom h-left',
					esc_html__('Top - Left', 'schon') => 'v-top h-left'
				),
				'std' => 'v-top h-right'
			),
			array(
				"type" => "dropdown",
				"heading" => esc_html__("Badge size", 'schon'),
				"param_name" => "badge_size",
				'group' => esc_html__('Badge', 'schon'),
				'value' => array(
					esc_html__('Small', 'schon') => 'badge-sm',
					esc_html__('Medium', 'schon') => 'badge-md',
					esc_html__('Large', 'schon') => 'badge-lg'
				),
				'std' => 'badge-md'
			),

			/*array(
				'type' => 'css_editor',
				'heading' => esc_html__('Css', 'js_composer'),
				'param_name' => 'css',
				'group' => esc_html__('Design options', 'js_composer'),
			),*/
		)
	));

	//[schon_title]
	vc_map(array(
		"name" => esc_html__("Title", 'schon'),
		"base" => "schon_title",
		"category" => 'schon',
		"params" => array(
			array(
				'type' => 'textfield',
				'holder' => 'div',
				'heading' => esc_html__('Title', 'schon'),
				'param_name' => 'title',
				'value' => '',
			),
			array(
				'type' => 'textfield',
				'holder' => 'div',
				'heading' => esc_html__('Subtitle', 'schon'),
				'param_name' => 'subtitle',
				'value' => '',
			),
			array(
				'type' => 'textfield',
				'heading' => esc_html__( 'Extra class name', 'js_composer' ),
				'param_name' => 'el_class',
				'description' => esc_html__( 'Style particular content element differently - add a class name and refer to it in custom CSS.', 'js_composer' ),
			),
			array(
				'type' => 'css_editor',
				'heading' => esc_html__('Css', 'js_composer'),
				'param_name' => 'css',
				'group' => esc_html__('Design options', 'js_composer'),
			)
		)
	));

	//[schon_products_carousel]
	vc_map(array(
		"name" => esc_html__("Products Carousel", 'schon'),
		"base" => "schon_products_carousel",
		"category" => 'schon',
		"params" => array(
			array(
				'type' => 'textfield',
				'heading' => esc_html__( 'Number of products to show', 'schon' ),
				'param_name' => 'number',
				'value' => '10'
			),
			array(
				"type" => "dropdown",
				"class" => "",
				"heading" => esc_html__("Show", 'schon'),
				"param_name" => "show",
				'value' => array(
					esc_html__('All products', 'schon') => '',
					esc_html__('Featured Products', 'schon') => 'featured',
					esc_html__('On sale products', 'schon') => 'onsale',
				),
			),

			array(
				'type' => 'textfield',
				'heading' => esc_html__( 'From categories', 'js_composer' ),
				'param_name' => 'categories',
				'description' => esc_html__( 'Insert category IDs separated with a comma.', 'schon' ),
			),

			array(
				"type" => "dropdown",
				"class" => "",
				"heading" => esc_html__("Order by", 'schon'),
				"param_name" => "orderby",
				'value' => array(
					esc_html__('Date', 'schon') => 'date',
					esc_html__('Price', 'schon') => 'price',
					esc_html__('Rand', 'schon') => 'rand',
					esc_html__('Sales', 'schon') => 'sales',
				),
			),

			array(
				"type" => "dropdown",
				"class" => "",
				"heading" => esc_html__("Order", 'schon'),
				"param_name" => "order",
				'value' => array(
					esc_html__('DESC', 'schon') => 'desc',
					esc_html__('ASC', 'schon') => 'asc',
				),
			),

			array(
				"type" => "checkbox",
				"heading" => esc_html__('Hide free products', 'schon'),
				"param_name" => "hide_free",
				'value' => '',
			),

			array(
				"type" => "checkbox",
				"heading" => esc_html__('Show hidden products', 'schon'),
				"param_name" => "show_hidden",
				'value' => '',
			),

			array(
				'type' => 'textfield',
				'heading' => esc_html__( 'Extra class name', 'js_composer' ),
				'param_name' => 'el_class',
				'description' => esc_html__( 'Style particular content element differently - add a class name and refer to it in custom CSS.', 'js_composer' ),
			),
			
			array(
				'type' => 'dropdown',
				'heading' => esc_html__( 'Number of columns', 'schon' ),
				'param_name' => 'number_columns',
				'value' => array(
					esc_html__('1', 'schon') => '1',
					esc_html__('2', 'schon') => '2',
					esc_html__('3', 'schon') => '3',
					esc_html__('4', 'schon') => '4',
					esc_html__('5', 'schon') => '5',
					esc_html__('6', 'schon') => '6',
				),
				'std' => '4',
				'group' => esc_html__( 'Layout', 'schon' ),
			),

			array(
				"type" => "dropdown",
				"heading" => esc_html__("Rows", 'schon'),
				"param_name" => "rows",
				'value' => array(
					esc_html__('1', 'schon') => '1',
					esc_html__('2', 'schon') => '2',
					esc_html__('3', 'schon') => '3',
					esc_html__('4', 'schon') => '4',
				),
				'group' => esc_html__( 'Layout', 'schon' ),
			),

			array(
				"type" => "dropdown",
				"heading" => esc_html__("Arrows position", 'schon'),
				"description" => esc_html__("The position of the navigation arrows", 'schon'),
				"param_name" => "arrows_position",
				'value' => array(
					esc_html__('Top', 'schon') => 'top',
					esc_html__('Center', 'schon') => 'center',
					esc_html__('Bottom', 'schon') => 'bottom',
				),
				'std' => 'top',
				'group' => esc_html__( 'Layout', 'schon' ),
			),

			array(
				'type' => 'css_editor',
				'heading' => esc_html__('Css', 'js_composer'),
				'param_name' => 'css',
				'group' => esc_html__('Design options', 'js_composer'),
			),
		)
	));

	//[schon_brands_carousel]
	vc_map(array(
		'name' => esc_html__('Brands Carousel','schon'),
		'base' => 'schon_brands_carousel',
		'category' => 'schon',
		'params' => array(
			array(
				"type" => "attach_images",
				"heading" => esc_html__("Images", 'schon'),
				"param_name" => "images",
				"value" => '', //Default Red color
			),
			array(
				'type' => 'dropdown',
				'heading' => esc_html__( 'Number of columns', 'schon' ),
				'param_name' => 'number_columns',
				'value' => array(
					esc_html__('3', 'schon') => '3',
					esc_html__('4', 'schon') => '4',
					esc_html__('5', 'schon') => '5',
					esc_html__('6', 'schon') => '6',
					esc_html__('7', 'schon') => '7',
					esc_html__('8', 'schon') => '8',
					esc_html__('9', 'schon') => '9',
					esc_html__('10', 'schon') => '10',
				),
				'std' => '6',
			),

			array(
				"type" => "dropdown",
				"heading" => esc_html__("Rows", 'schon'),
				"param_name" => "rows",
				'value' => array(
					esc_html__('1', 'schon') => '1',
					esc_html__('2', 'schon') => '2',
					esc_html__('3', 'schon') => '3',
					esc_html__('4', 'schon') => '4',
				),
			),

			array(
				"type" => "dropdown",
				"heading" => esc_html__("Arrows position", 'schon'),
				"description" => esc_html__("The position of the navigation arrows", 'schon'),
				"param_name" => "arrows_position",
				'value' => array(
					esc_html__('Top', 'schon') => 'top',
					esc_html__('Center', 'schon') => 'center',
					esc_html__('Bottom', 'schon') => 'bottom',
				),
				'std' => 'top',
			),
			
			array(
				'type' => 'textfield',
				'heading' => esc_html__( 'Extra class name', 'js_composer' ),
				'param_name' => 'el_class',
				'description' => esc_html__( 'Style particular content element differently - add a class name and refer to it in custom CSS.', 'js_composer' ),
			),
			array(
				'type' => 'css_editor',
				'heading' => esc_html__('Css', 'js_composer'),
				'param_name' => 'css',
				'group' => esc_html__('Design options', 'js_composer'),
			)


		)
	));

	//[schon_info_box]
	vc_map(array(
		"name" => esc_html__("Info Box", 'schon'),
		"base" => "schon_info_box",
		"category" => 'schon',
		"params" => array(

			array(
				'type' => 'iconpicker',
				'heading' => __( 'Icon', 'schon' ),
				'param_name' => 'icon',
				'value' => 'fa fa-arrow-circle-o-right',
				'settings' => array(
					'emptyIcon' => true,
					// default true, display an "EMPTY" icon?
					'type' => 'fontawesome',
				),
			),
			array(
				'type' => 'dropdown',
				'heading' => esc_html__( 'Layout', 'schon' ),
				'param_name' => 'layout',
				'value' => array(
					esc_html__( 'Horizontal', 'schon' ) => 'horizontal',
					esc_html__( 'Vertical', 'schon' ) => 'vertical',
				)
			),
			array(
				'type' => 'textfield',
				'heading' => esc_html__( 'Title', 'schon' ),
				'param_name' => 'title',
			),
			array(
				'type' => 'textarea',
				'heading' => esc_html__( 'Content', 'schon' ),
				'param_name' => 'subtitle',
			),
			array(
				'type' => 'textfield',
				'heading' => esc_html__( 'Extra class name', 'js_composer' ),
				'param_name' => 'el_class',
				'description' => esc_html__( 'Style particular content element differently - add a class name and refer to it in custom CSS.', 'js_composer' ),
			),
			array(
				'type' => 'css_editor',
				'heading' => esc_html__('Css', 'js_composer'),
				'param_name' => 'css',
				'group' => esc_html__('Design options', 'js_composer'),
			),
		)
	));

	//[schon_button]
	vc_map(array(
		"name" => esc_html__("Button", 'schon'),
		"base" => "schon_button",
		"category" => 'schon',
		"params" => array(
			array(
				'type' => 'textfield',
				'heading' => esc_html__( 'Link', 'schon' ),
				'param_name' => 'link',
				'value' => '#'
			),
			array(
				'type' => 'textfield',
				'heading' => esc_html__( 'Text', 'schon' ),
				'param_name' => 'text',
				'value' => 'Text'
			),
			array(
				"type" => "dropdown",
				"heading" => esc_html__("Style", 'schon'),
				"param_name" => "style",
				'value' => array(
					esc_html__('Just text', 'schon') => 'no-padding',
					esc_html__('Default', 'schon') => 'btn-default',
					esc_html__('Default ghost', 'schon') => 'btn-default btn-ghost',
					esc_html__('Primary', 'schon') => 'btn-primary',
					esc_html__('Primary ghost', 'schon') => 'btn-primary btn-ghost',
					esc_html__('Secondary', 'schon') => 'btn-primary-2',
					esc_html__('Secondary ghost', 'schon') => 'btn-primary-2 btn-ghost',
					esc_html__('Info', 'schon') => 'btn-info',
					esc_html__('Info ghost', 'schon') => 'btn-info btn-ghost',
					esc_html__('Success', 'schon') => 'btn-success',
					esc_html__('Success ghost', 'schon') => 'btn-success btn-ghost',
					esc_html__('Warning', 'schon') => 'btn-warning',
					esc_html__('Warning ghost', 'schon') => 'btn-warning btn-ghost',
					esc_html__('Danger', 'schon') => 'btn-danger',
					esc_html__('Danger ghost', 'schon') => 'btn-danger btn-ghost',
				),
				'std' => 'btn-default'
			),
			array(
				"type" => "dropdown",
				"heading" => esc_html__("Size", 'schon'),
				"param_name" => "size",
				'value' => array(
					esc_html__('Very small', 'schon') => 'btn-xs',
					esc_html__('Small', 'schon') => 'btn-sm',
					esc_html__('Medium', 'schon') => ' ',
					esc_html__('Large', 'schon') => 'btn-lg',
				),
				'std' => ' ',
			),
			array(
				"type" => "dropdown",
				"heading" => esc_html__("Display", 'schon'),
				"param_name" => "display",
				'value' => array(
					esc_html__('Inline', 'schon') => '',
					esc_html__('Block', 'schon') => 'btn-block',
				),
			),
			array(
				'type' => 'iconpicker',
				'heading' => __( 'Icon', 'schon' ),
				'param_name' => 'icon',
				'value' => '',
				'settings' => array(
					'emptyIcon' => true,
					// default true, display an "EMPTY" icon?
					'type' => 'fontawesome',
				),
			),
			array(
				'type' => 'textfield',
				'heading' => esc_html__( 'Extra class name', 'js_composer' ),
				'param_name' => 'el_class',
				'description' => esc_html__( 'Style particular content element differently - add a class name and refer to it in custom CSS.', 'js_composer' ),
			),
			array(
				'type' => 'css_editor',
				'heading' => esc_html__('Css', 'js_composer'),
				'param_name' => 'css',
				'group' => esc_html__('Design options', 'js_composer'),
			),
		)
	));

	//[schon_team_member]
	vc_map(array(
		"name" => esc_html__("Team member", 'schon'),
		"base" => "schon_team_member",
		"category" => 'schon',
		"params" => array(
			array(
				'type' => 'attach_image',
				'heading' => esc_html__( 'Avatar', 'schon' ),
				'param_name' => 'avatar',
			),
			array(
				'type' => 'textfield',
				'heading' => esc_html__( 'Name', 'schon' ),
				'param_name' => 'name',
			),
			array(
				'type' => 'textfield',
				'heading' => esc_html__( 'Role', 'schon' ),
				'param_name' => 'role',
			),
			array(
				'type' => 'textfield',
				'heading' => esc_html__( 'Facebook', 'schon' ),
				'param_name' => 'facebook',
			),
			array(
				'type' => 'textfield',
				'heading' => esc_html__( 'Twitter', 'schon' ),
				'param_name' => 'twitter',
			),
			array(
				'type' => 'textfield',
				'heading' => esc_html__( 'Linkedin', 'schon' ),
				'param_name' => 'linkedin',
			),
			array(
				'type' => 'textfield',
				'heading' => esc_html__( 'Instagram', 'schon' ),
				'param_name' => 'instagram',
			),
			array(
				'type' => 'textfield',
				'heading' => esc_html__( 'Extra class name', 'js_composer' ),
				'param_name' => 'el_class',
				'description' => esc_html__( 'Style particular content element differently - add a class name and refer to it in custom CSS.', 'js_composer' ),
			),
			array(
				'type' => 'css_editor',
				'heading' => esc_html__('Css', 'js_composer'),
				'param_name' => 'css',
				'group' => esc_html__('Design options', 'js_composer'),
			),
		)
	));
}
add_action( 'vc_before_init', 'schon_integrateWithVC' );

if(class_exists('WPBakeryShortCode')) {
	class WPBakeryShortCode_schon_banner extends WPBakeryShortCode {
	}

	class WPBakeryShortCode_schon_title extends WPBakeryShortCode {
	}

	class WPBakeryShortCode_schon_products_carousel extends WPBakeryShortCode {
	}

	class WPBakeryShortCode_schon_brands_carousel extends WPBakeryShortCode {
	}

	class WPBakeryShortCode_schon_info_box extends WPBakeryShortCode {
	}

	class WPBakeryShortCode_schon_button extends WPBakeryShortCode {
	}
	class WPBakeryShortCode_schon_team_member extends WPBakeryShortCode {
	}
}