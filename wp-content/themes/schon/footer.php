<?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the #content div and all content after
 *
 * @package schon
 */
?>

	</div><!-- #content -->

<?php
$accordion_enabled = schon_get_option('footer-widgets-accordion-enabled');
$accordion_class = ($accordion_enabled) ? 'schon-accordion-enabled' : '';
?>

	<footer id="colophon" class="site-footer <?php echo $accordion_class;?>" role="contentinfo">

		<?php if(is_active_sidebar('footer-row-1')):?>
			<div id="footer-widgets-row-1" class="footer-widgets-row">
				<div class="container">
					<div class="row">
						<?php dynamic_sidebar( 'footer-row-1' ) ?>
					</div>
				</div>
			</div>
		<?php endif; ?>

		<?php if(is_active_sidebar('footer-row-2')):?>
			<div id="footer-widgets-row-2" class="footer-widgets-row">
				<div class="container">
					<div class="row">
						<?php dynamic_sidebar( 'footer-row-2' ) ?>
					</div>
				</div>
			</div>
		<?php endif; ?>

		<?php if(schon_get_option('copy-social-bar')): ?>
		<div id="site-info">
			<div class="container">

				<?php
				$footer_left_content = schon_get_option('footer-copyright-left');
				if ($footer_left_content): ?>
					<div class="footer-copyright-content footer-copyright-left">
						<?php echo do_shortcode($footer_left_content); ?>
					</div>
				<?php endif; ?>

				<?php
				$footer_center_content = schon_get_option('footer-copyright-center');
				if ($footer_center_content): ?>
					<div class="footer-copyright-content footer-copyright-center">
						<?php echo do_shortcode($footer_center_content); ?>
					</div>
				<?php endif; ?>

				<?php
				$footer_right_content = schon_get_option('footer-copyright-right');
				if ($footer_right_content): ?>
					<div class="footer-copyright-content footer-copyright-right">
						<?php echo do_shortcode($footer_right_content); ?>
					</div>
				<?php endif; ?>

			</div><!-- .container -->
		</div><!-- .site-info -->
		<?php endif; ?>
	</footer><!-- #colophon -->

</div><!-- #page -->

<?php wp_footer(); ?>

</body>
</html>