<?php
// Attributes
$css_class = '';
if(function_exists('vc_map_get_attributes')) {
	$atts = vc_map_get_attributes( 'schon_team_member', $atts );
	$css_class .= apply_filters( VC_SHORTCODE_CUSTOM_CSS_FILTER_TAG, vc_shortcode_custom_css_class( $atts['css'], ' ' ), $this->settings['base'], $atts );
} else {
	$atts = shortcode_atts(
		array(
			'avatar' => '',
			'name' => '',
			'role' => '',

			'facebook' => '',
			'twitter' => '',
			'linkedin' => '',
			'instagram' => '',

			'el_class' => '',
			'css' => '',
		),
		$atts,
		'schon_team_member'
	);
}

$css_class .= ' ' . $atts['el_class'];

$avatar_url =  wp_get_attachment_image_src( $atts['avatar'], 'medium' );

$facebook = (!empty($atts['facebook'])) ? '<a class="facebook" href="'.$atts['facebook'].'"><i class="fa fa-facebook"></i></a>' : '';
$twitter = (!empty($atts['twitter'])) ? '<a class="twitter" href="'.$atts['twitter'].'"><i class="fa fa-twitter"></i></a>' : '';
$linkedin = (!empty($atts['linkedin'])) ? '<a class="linkedin" href="'.$atts['linkedin'].'"><i class="fa fa-linkedin"></i></a>' : '';
$instagram = (!empty($atts['instagram'])) ? '<a class="instagram" href="'.$atts['instagram'].'"><i class="fa fa-instagram"></i></a>' : '';

?>

<div class="schon-team-member <?php echo esc_attr($css_class); ?>">
	<?php
	if (!empty($avatar_url)): ?>
	<div class="member-avatar">
		<img src="<?php echo $avatar_url[0]; ?>">
		<?php if(!empty($facebook) || !empty($twitter) || !empty($linkedin) || !empty($instagram)): ?>
		<div class="member-overlay"></div>
		<div class="socials-wrap">
			<div class="socials-wrap-inner">
				<?php
				echo $facebook,$twitter,$linkedin,$instagram;
				?>
			</div>
		</div>
		<?php endif; ?>
	</div>
	<?php endif; ?>
	<div class="informations-container">
		<h3 class="member-team-name"><?php echo $atts['name']; ?></h3>
		<div class="member-team-role"><?php echo $atts['role']; ?></div>
	</div>
</div>
