<?php
$css_class = '';
if(function_exists('vc_map_get_attributes')) {
	$atts = vc_map_get_attributes( 'schon_brands_carousel', $atts );
	$css_class .= apply_filters( VC_SHORTCODE_CUSTOM_CSS_FILTER_TAG, vc_shortcode_custom_css_class( $atts['css'], ' ' ), $this->settings['base'], $atts );
} else {
	$atts = shortcode_atts(
		array(
			'images' => '',
			'number_columns' => 4,
			'rows' => 1,
			'arrows_position' => 'center',

			'el_class' => '',
			'css' => '',
		),
		$atts,
		'schon_brands_carousel'
	);
}

$item_id = 'schon-brands-';
$item_id .= schon_get_rand();

$css_class .= ' ' . $atts['el_class'];

$navigation_class = 'navigation-'.$atts['arrows_position'];

?>

<?php if(!empty($atts['images'])): ?>
<div class="schon-brands-carousel <?php echo $item_id . ' ' .$css_class; ?>">
	<div class="schon-brands-carousel-inner schon-owl-theme">
		<div class="schon-brands-carousel-inner owl-brands <?php echo esc_attr($navigation_class); ?>">
		
			<?php
			$c = 0;
			foreach(explode(",", $atts['images']) as $image){
				if(function_exists('wpb_getImageBySize')) {
					if ( $image > 0 ) {
						$post_thumbnail = wpb_getImageBySize( array( 'attach_id'  => $image, 'thumb_size' => 'medium',
						) );
					} else {
						$post_thumbnail                   = array();
						$post_thumbnail['thumbnail']      = '<img class="img-thumbnail" src="' . vc_asset_url( 'vc/no_image.png' ) . '" />';
						$post_thumbnail['p_img_large'][0] = vc_asset_url( 'vc/no_image.png' );
					}
				} else {
					$post_thumbnail = array();
					$post_thumbnail_url =  wp_get_attachment_image_src( $image, 'medium' );
					$post_thumbnail['thumbnail'] = '<img src="' . $post_thumbnail_url[0] . '" />';
				}

				$thumbnail = $post_thumbnail['thumbnail'];

				$div_start = '<div class="schon-brand-logo">';
				$div_end = '</div>';

				if($c++ == 0) {
					echo '<div class="item">';
				}

				echo $div_start . $thumbnail . $div_end;

				if($c == $atts['rows']) {
					echo '</div>';
					$c = 0;
				}
			}
			?>
		
		</div>
	</div>
</div>
<?php endif; ?>

<script>
	(function($){
		'use strict';
		$(window).load(function() {
			var $owl_brands = $('.<?php echo $item_id; ?> .owl-brands');

			function fix_multirows_items() {
				var $products = $owl_brands.find('.item .schon-brand-logo');

				$products.height('auto');

				var maxHeight = Math.max.apply(null, $products.map(function ()
				{
					return $(this).height();
				}).get());

				$products.height(maxHeight);
			}


			if ($owl_brands.length > 0) {

				$($owl_brands).owlCarousel({
					'items': <?php echo $atts['number_columns']; ?>,
					'pagination': false,
					'navigation': true,

					'afterUpdate' : fix_multirows_items,
					'afterInit' : fix_multirows_items
				});
			}
		});
	})(jQuery);

</script>
