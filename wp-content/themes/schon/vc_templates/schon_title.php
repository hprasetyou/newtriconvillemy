<?php
// Attributes
$css_class = '';
if(function_exists('vc_map_get_attributes')) {
	$atts = vc_map_get_attributes( 'schon_title', $atts );
	$css_class .= apply_filters( VC_SHORTCODE_CUSTOM_CSS_FILTER_TAG, vc_shortcode_custom_css_class( $atts['css'], ' ' ), $this->settings['base'], $atts );
} else {
	$atts = shortcode_atts(
		array(
			'title' => '',
			'subtitle' => '',

			'el_class' => '',
			'css' => '',
		),
		$atts,
		'schon_title'
	);
}


$css_class .= ' ' . $atts['el_class'];
?>

<div class="schon-title-wrap <?php echo esc_attr($css_class) ?>" >
	<h2 class="schon-title-title"><?php echo $atts['title']; ?></h2>

	<?php if(!empty($atts['subtitle'])): ?>
	<h5 class="schon-title-subtitle"><?php echo $atts['subtitle']; ?></h5>
	<?php endif; ?>

</div>