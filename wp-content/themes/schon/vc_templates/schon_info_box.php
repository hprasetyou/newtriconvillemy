<?php
$css_class = '';
if(function_exists('vc_map_get_attributes')) {
	$atts = vc_map_get_attributes( 'schon_info_box', $atts );
	$css_class .= apply_filters( VC_SHORTCODE_CUSTOM_CSS_FILTER_TAG, vc_shortcode_custom_css_class( $atts['css'], ' ' ), $this->settings['base'], $atts );
} else {
	$atts = shortcode_atts(
		array(
			'icon' => '',
			'title' => '',
			'subtitle' => '',
			'layout' => 'horizontal',

			'el_class' => '',
			'css' => '',
		),
		$atts,
		'schon_info_box'
	);
}

?>

<div class="schon-featured-info <?php echo esc_attr($atts['el_class']) . ' ' . esc_attr($atts['layout']) . ' ' . esc_attr($css_class) ?>">
	<div class="schon-featured-info-icon-wrap">
		<i class="fa <?php echo esc_attr($atts['icon']) ?>" aria-hidden="true"></i>
	</div>
	<div class="schon-featured-info-details">
		<p class="schon-featured-info-title"> <?php echo $atts['title'] ?></p>
		<div class="schon-featured-info-subtitle"><?php echo $atts['subtitle'] ?></div>
	</div>
</div>
