<?php

$css_class = '';
if(function_exists('vc_map_get_attributes')) {
	$atts = vc_map_get_attributes( 'schon_button', $atts );
	$css_class .= apply_filters( VC_SHORTCODE_CUSTOM_CSS_FILTER_TAG, vc_shortcode_custom_css_class( $atts['css'], ' ' ), $this->settings['base'], $atts );
} else {
	$atts = shortcode_atts(
		array(
			'text' => 'Text',
			'link' => '#',
			'style' => 'btn-default',
			'size' => '',
			'display' => '',
			'icon' => '',

			'el_class' => '',
			'css' => '',
		),
		$atts,
		'schon_button'
	);
}


$css_class .= ' ' . $atts['el_class'];

$css_class .= ' ' . $atts['style'];
$css_class .= ' ' . $atts['size'];
$css_class .= ' ' . $atts['display'];

$icon = (!empty($atts['icon'] )) ? '<i class="'.$atts['icon'].'"></i>': '';
echo '<a href="'.$atts['link'] .'" class="btn '.esc_attr($css_class) .'">'.$atts['text'].' '. $icon .'</a>';
?>
