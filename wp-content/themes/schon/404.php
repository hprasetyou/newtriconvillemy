<?php
/**
 * The template for displaying 404 pages (not found).
 *
 * @package schon
 */

get_header(); ?>

	<div class="container">
		<div class="row">
			
			<div id="primary" class="content-area <?php echo schon_get_primary_class() ?>">
				<main id="main" class="site-main" role="main">

					<section class="error-404 not-found">

						<header class="page-header">
							<h1 class="page-title"><?php esc_html_e( 'Oops! That page can&rsquo;t be found.', 'schon' ); ?></h1>
						</header><!-- .page-header -->

						<div class="page-content">

							<p><?php esc_html_e( 'It looks like nothing was found at this location. Maybe try one of the links below or a search?', 'schon' ); ?></p>

							<div class="margin-30"></div>

							<?php get_search_form(); ?>



						</div><!-- .page-content -->
					</section><!-- .error-404 -->

				</main><!-- #main -->
			</div><!-- #primary -->
			

			<?php schon_show_sidebar(); ?>

		</div><!-- .row -->
	</div><!-- .container -->

<?php get_footer(); ?>
