<?php
/**
 * The template for displaying woocommerce pages.
 *
 *
 * @package schon
 */

get_header(); ?>

	<div class="container">
		<div class="row">

			<div id="primary" class="content-area <?php echo is_product_category()?'tc-product-grid':'' ?> <?php echo schon_get_primary_class('sidebar-woocommerce') ?>">
				<main id="main" class="site-main" role="main">

					<?php woocommerce_content() ?>

				</main><!-- #main -->
			</div><!-- #primary -->
			

			<?php schon_show_sidebar('sidebar-woocommerce'); ?>

		</div><!-- .row -->
	</div><!-- .container -->

<?php get_footer(); ?>
