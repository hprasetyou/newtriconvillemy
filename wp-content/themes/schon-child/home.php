<?php
/**
 * The main template file.
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 * Learn more: http://codex.wordpress.org/Template_Hierarchy
 *
 * @package schon
 */

get_header(); 
$popularpost  = new WP_Query( array( 'posts_per_page' => 1, 'order' => 'DESC'  ) );
//print_r($popularpost);

?>

<div class="container">
    <div class="row">
        
        <div id="primary" class="content-area  <?php echo schon_get_primary_class('sidebar-blog') ?>">
				<?php
				$main_classes = apply_filters('schon_main_classes', '');
				?>
            <main id="main" class="site-main <?php echo esc_attr($main_classes) ?>" role="main">
                
                <div class="blog-posts-container inner-container clearfix">
                    <div id="top-post" class="clearfix">
                                        <?php
                                        while ( $popularpost->have_posts() ) {
                                            if ( has_post_thumbnail() ) {
                                                    $medium_src = wp_get_attachment_image_src( get_post_thumbnail_id(), 'medium' );
                                                    $large_src = wp_get_attachment_image_src( get_post_thumbnail_id(), 'large' );
                                                    $full_src = wp_get_attachment_image_src( get_post_thumbnail_id(), 'full' );

                                                    echo '<picture>';
                                                    echo '<source srcset="'.esc_url($full_src[0]).'" media="(min-width: 641px)" >';
                                                    echo '<source srcset="'.esc_url($large_src[0]).'" media="(min-width: 301px)" >';
                                                    echo '<img src="'.$full_src[0].'">';
                                                    echo '</picture>';
                                            }
                                          $popularpost->the_post();
                                          echo '<div class="col-sm-8 col-sm-offset-4">';
                                          the_title('<h1 class="entry-title"><a href="' . esc_url( get_permalink() ) . '" rel="bookmark">', '</a></h1>');
                                          the_excerpt();
                                          echo '<p class="more-para"><a href="' . esc_url( get_permalink() ) . '" class="more-link"><span class="fa fa-caret-right"></span> Read More</a></p>';
                                          echo '</div>';
                                        }
                                        ?>
                        
                    </div>
                    <div class="grid-sizer"></div>
                    <div class="gutter-sizer"></div>
						<?php if ( have_posts() ) : ?>
							<?php 
                                                                $i = 1;
                                                                echo '<div class="row">';
                                                                while ( have_posts() ) : the_post();
                                                                        /* Include the Post-Format-specific template for the content.
                                                                         * If you want to override this in a child theme, then include a file
                                                                         * called content-___.php (where ___ is the Post Format name) and that will be used instead.
                                                                         */
                                                                    get_template_part( 'content', get_post_format() ); 
                                                                    if ( $i % 3 === 0 ) { echo '</div><div class="row">'; }
                                                                    $i++; 
                                                                endwhile; // end of the loop.
                                                                echo ' </div>';
                                                        ?>
                </div>
						<?php schon_paginate_links(); ?>
                
					<?php else : ?>
                
						<?php get_template_part( 'content', 'none' ); ?>
                
					<?php endif; ?>
                
            </main><!-- #main -->
        </div><!-- #primary -->
        
        
			<?php schon_show_sidebar('sidebar-blog'); ?>
        
    </div><!-- .row -->
</div><!-- .container -->


<?php get_footer(); ?>
