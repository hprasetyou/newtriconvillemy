<?php
update_option('siteurl', 'https://dev.triconville.com.my');
update_option('home', 'https://dev.triconville.com.my');
function schon_page_title_bar($title, $subtitle)
{
    $content_type = 'none';
    $extra_classes = '';

    $alignment = schon_get_option('page-title-bar-alignment');
    $background_url = schon_get_titlebar_background();
    $background_image = (empty($background_url)) ? '' : 'background-image:url("'.esc_url($background_url).'");';

    $color_style = schon_get_option('titlebar_color_style');
    if (!$subtitle) {
        $args = array( 'taxonomy' => 'product_cat' );
        $terms = get_terms('product_cat', $args);
        $count = count($terms);
        if ($count > 0) {
            foreach ($terms as $term) {
                $subtitle = $term->description;
            }
        }
    }

    if (!is_product()) {
        ?>
                    <div class="container">
			<div id="page-title-bar" class="page-title-bar-content-<?php echo esc_attr($content_type) . ' ' . esc_attr($alignment) . ' ' . esc_attr($color_style); ?>" style="<?php echo esc_attr($background_image); ?>">
				<div class="titlebar-overlay"></div>
				<div class="container">
					<?php if ($title): ?>
						<h1 class="entry-title"><?php echo $title; ?></h1>
						<?php if ($subtitle): ?>
						<div class="h4 entry-subtitle"><?php echo esc_html($subtitle); ?></div>
						<?php endif; ?>
					<?php endif; ?>

				</div>
			</div>
                    </div>
		<?php
    }
}
if (!function_exists('schon_get_primary_class')) {
    /**
     * Get primary column class
     * @param string name
     * @return string
     */
    function schon_get_primary_class($name = null)
    {
        if ($name == 'sidebar-blog') {
            return 'col-sm-12';
        }
        $sidebarOption = schon_get_sidebar_option($name);

        $secondaryWidth = 2;

        if ($sidebarOption == 1 || $sidebarOption == 4) {
            $primaryWidthClass = 'col-md-12';
        } else {
            $primaryWidthClass = ' col-md-' . (12 - $secondaryWidth) . ' col-sm-' . (12 - ($secondaryWidth+1));
        }

        if ($sidebarOption == 3) {
            $primaryWidthClass .= ' col-md-push-' . $secondaryWidth . ' col-sm-push-' . ($secondaryWidth+1);
        }

        return $primaryWidthClass;
    }
}

function wpdocs_custom_excerpt_length($length)
{
    return 20;
}
function new_excerpt_more($more)
{
    return '...';
}
add_filter('excerpt_more', 'new_excerpt_more');
add_filter('excerpt_length', 'wpdocs_custom_excerpt_length', 999);

if (!function_exists('schon_get_secondary_class')) {
    /**
     * Get secondary column width
     * @param string $name
     * @return string|null
     */
    function schon_get_secondary_class($name = null)
    {
        $sidebarOption = schon_get_sidebar_option($name);

        $secondaryWidth = 2;

        if ($sidebarOption == 1) {
            $secondaryWidthClass = null;
        } else {
            $secondaryWidthClass = ' col-md-' . $secondaryWidth . ' col-sm-' . ($secondaryWidth+1);
        }

        if ($sidebarOption == 3) {
            $secondaryWidthClass .= ' col-md-pull-' . (12 - $secondaryWidth) . ' col-sm-pull-' . (12 - ($secondaryWidth+1));
        }

        return $secondaryWidthClass;
    }
}

function schon_get_titlebar_background()
{

        //if its a "single item".
    //Necessary until metabox.io don't avoid to get the first prost information on the taxconomy page
    if (is_single() || is_product() || is_page()) {
        $background = schon_get_meta_option('schon_meta_titlebar-background-image');
        if (! empty($background)) {
            $background = array_values($background);
            $background = array_shift($background);

            return $background['url'];
        }
    }
    if (is_product_category()) {
        global $wp_query;

        // get the query object
        $cat = $wp_query->get_queried_object();

        // get the thumbnail id using the queried category term_id
        $thumbnail_id = get_term_meta($cat->term_id, 'thumbnail_id', true);

        // get the image URL
        $image = wp_get_attachment_url($thumbnail_id);
        if ($image) {
            return $image;
        }
        // print the IMG HTML
    }

    //Get the taxonomy to use for the current post type
    $post_type = get_post_type();
    $taxonomy = '';
    switch ($post_type) {
            case 'post':
                $taxonomy = 'category';
                break;
            case 'product':
                $taxonomy = 'product_cat';
                break;
        }

    if (!empty($taxonomy)) {
        global $post;

        if (is_single() || is_product()) {
            $categories = get_the_terms($post->ID, $taxonomy);
            $term_id = (!empty($categories)) ? $categories[count($categories)-1]->term_id : null;
        } else {
            $categories = get_term_by('term_name', get_queried_object_id(), $taxonomy);
            $term_id = (!empty($categories)) ? $categories->term_id : null;
        }

//                        echo $term_id;
        if (function_exists('z_taxonomy_image_url') && !is_null($term_id)) {
            //$taxonomy_background = schon_get_term_meta_option('schon_meta_titlebar_background_image');
            $taxonomy_background = z_taxonomy_image_url($term_id);
            if (!empty($taxonomy_background)) {
                return $taxonomy_background;
            }
        }

        //Todo: recursive in case of nested terms
    }


    $default_image = schon_get_option('titlebar-default-image');


    return $default_image['url'];
}

function wp_67472455()
{
    wp_dequeue_style('schon');
    wp_dequeue_style('schon-parent');
    wp_dequeue_style('schon-child');

    remove_theme_support('wc-product-gallery-slider');
    wp_dequeue_style('owl-carousel');
    wp_dequeue_style('owl-carousel-theme');
    wp_dequeue_style('owl-carousel-transitions');
    wp_dequeue_script('vc_pageable_owl-carousel');
    wp_dequeue_script('owl-carousel');

    wp_enqueue_style('schon-style', get_template_directory_uri() . '/../schon-child/public/style.css');
}
add_action('wp_print_styles', 'wp_67472455', 100);

add_action('wp_head', 'remove_my_action');

add_action('woocommerce_before_main_content', 'woocommerce_taxonomy_archive_description');
function remove_my_action()
{
    remove_action('woocommerce_before_shop_loop_item_title', 'schon_woocommerce_template_loop_product_thumbnail');
    remove_action('woocommerce_archive_description', 'woocommerce_taxonomy_archive_description');
    remove_action('woocommerce_single_product_summary', 'schon_single_product_wishlist_link', 21);
    remove_action('woocommerce_login_form_start', 'schon_render_social_buttons');
    remove_action('woocommerce_single_product_summary', 'woocommerce_template_single_add_to_cart', 15);
}
if (!function_exists('schon_render_social_buttons')) {
    /**
     * Render the social login buttons on My Account Page
     */
    function schon_render_social_buttons()
    {
        if ((function_exists('is_account_page')) && !is_account_page() || !Sc_Facebook_Signing::isEnabled() && !Sc_Google_Signing::isEnabled()) {
            return;
        }

        echo '<div class="my_account_login-social-buttons">';
        if (Sc_Facebook_Signing::isEnabled()) {
            echo '<a href="' . Sc_Facebook_Signing::getLoginUrl() . '" class="btn btn-default btn-facebook"><i class="fa fa-facebook"></i> ' . esc_html__('Log in / Register with Facebook', 'schon') .'</a>';
        }

        if (Sc_Google_Signing::isEnabled()) {
            echo '<a href="' . Sc_Google_Signing::getLoginUrl() . '" class="btn btn-default btn-google"><i class="fa fa-google-plus"></i> ' . esc_html__('Log in / Register with Google', 'schon') .'</a>';
        }
        echo '</div>';
    }
}
add_action('woocommerce_register_form_start', 'schon_render_social_buttons');

function delivery_note()
{
    wc_get_template('single-product/shipping-info.php');
}

function schon_add_buttons_woocommerce_list()
{
    global $product;

    if (! class_exists('WC_List_Grid')) {
        return;
    }

    if (schon_is_woocommerce_active() && (is_shop() || is_product_category() || is_product_tag() || is_product_taxonomy())) {
        if (schon_is_wishlist_enabled() && schon_get_option('show-wishlist-button-loop')) {
            $add_to_wishlist = schon_get_add_to_wishlist_url($product->get_id());
            $label           = '';//( $add_to_wishlist['added'] ) ? esc_html__( 'View wishlist', 'schon' ) : esc_html__( 'Add to wishlist', 'schon' );

            echo '<a class="link add-to-wishlist ' . $add_to_wishlist['wishlist_class'] . '" href="' . $add_to_wishlist['wishlist_url'] . '"><i class="fa fa-heart-o"></i> <span class="atl-label">' . $label . '</span></a>';
        }

        if (schon_is_compare_enabled() && schon_get_option('show-compare-button-loop')) {
            $add_to_compare = schon_get_add_to_compare_url($product->get_id());
            $label          = '';//( $add_to_compare['added'] ) ? esc_html__( 'View compare table', 'schon' ) : esc_html__( 'Compare', 'schon' );
            echo '<a data-product_id="' . $product->get_id() . '" class="link add-to-compare compare ' . $add_to_compare['compare_class'] . '" href="' . $add_to_compare['compare_url'] . '"><i class="fa fa-exchange"></i> <span class="atl-label">' . $label . '</span></a>';
        }

        if (schon_is_quickview_enabled() && schon_get_option('show-quickview-button-loop')) {
            echo '<a href="#" class="link yith-wcqv-button quickview" data-product_id="' . $product->get_id() . '"><i class="icon icon-eye"></i> ' . esc_html__('QuickView', 'schon') . '</a>';
        }
    }
}
function visit_studio()
{
    include 'woocommerce/single-product/banner/visit-studio-banner.php';
}

function check_stock_product_detail()
{
    wc_get_template('single-product/stock-info.php');
}
add_action('woocommerce_single_product_summary', 'schon_single_product_wishlist_link', 16); //11
add_action('woocommerce_single_product_summary', 'delivery_note', 18); //11
add_action('woocommerce_single_product_summary', 'check_stock_product_detail', 14); //11
add_action('woocommerce_single_product_summary', 'woocommerce_template_single_add_to_cart', 17);
add_action('woocommerce_after_single_product_summary', 'visit_studio', 21);
function add_bracket()
{
    echo '<div class="woocommerce-product-bracket">';
    echo '<div class="col-sm-3">';

    global $product;
    $c = 1;

    $quickview_button = '';
    if (schon_is_quickview_enabled() && schon_get_option('show-quickview-button-loop')) {
        $c ++;

        //$quickview_button = '<a href="#" class="button yith-wcqv-button quickview" data-product_id="' . $product->get_id() . '"><i class="icon icon-eye"></i></a>';
        $quickview_button = '<a href="#" class="btn yith-wcqv-button quickview" data-product_id="' . $product->get_id() . '"></a>';
    }

    $wishlist_button = '';
    if (schon_is_wishlist_enabled() && schon_get_option('show-wishlist-button-loop')) {
        $c ++;

        $add_to_wishlist = schon_get_add_to_wishlist_url($product->get_id());

        $wishlist_button = '<a class="btn add-to-wishlist ' . $add_to_wishlist['wishlist_class'] . '" href="' . $add_to_wishlist['wishlist_url'] . '"></a>';
    }

    $compare_button = '';
    if (schon_is_compare_enabled() && schon_get_option('show-compare-button-loop')) {
        $c ++;

        $add_to_compare = schon_get_add_to_compare_url($product->get_id());

        $compare_button = '<a data-product_id="' . $product->get_id() . '" class="btn add-to-compare compare ' . $add_to_compare['compare_class'] . '" href="' . $add_to_compare['compare_url'] . '"></a>';
    }

    $additional_classes = 'buttons-' . $c;

    echo '</a><!-- /.woocommerce-product-image-wrap--><div class="add-to-cart-buttons-container ' . $additional_classes . '">';
    woocommerce_template_loop_add_to_cart();

    echo $quickview_button . $wishlist_button . $compare_button . '</div>';

    $align_text = schon_get_option('product-align-text');
    echo '</div><!-- add-to-cart-buttons-container --><div class="product-link-decription-container '.$align_text.'"><a href="' . get_the_permalink() . '" class="woocommerce-LoopProduct-link">';
    echo '</div><div class="col-sm-9 brace-product-space"></div></div>';
}

add_action('woocommerce_before_shop_loop_item', 'add_bracket');



add_action('wp_enqueue_scripts', 'schon_child_theme_enqueue_styles', 99);
function schon_child_theme_enqueue_styles()
{
    $style_require = array();
    if (wp_script_is('grid-list-layout')) {
        array_push($style_require, 'grid-list-layout');
    }
    if (wp_script_is('grid-list-layout')) {
        array_push($style_require, 'grid-list-button');
    }

    array_push($style_require, 'bootstrap');


    wp_enqueue_style('schon-parent', get_template_directory_uri() . '/style.css', $style_require);
    wp_dequeue_style('theme-options');
    wp_enqueue_style('theme-options', get_template_directory_uri() . "/css/theme-options.css", array('schon-parent'));
    wp_enqueue_style('schon-child', get_stylesheet_uri(), array('schon', 'theme-options'));
    wp_enqueue_script('schon-custom', get_template_directory_uri() . '/../schon-child/public/bundle.js', array('jquery'), false, false);
    wp_enqueue_script('schon-custom', get_template_directory_uri() . '/../schon-child/js/test.js', array('jquery'), false, false);

    //	add_action( 'woocommerce_after_add_to_cart_button', 'content_after_addtocart_button_func' );




    function content_after_addtocart_button_func()
    {
        // Echo content.
        echo '<div  style="font-size:18px; font-weight:bold;text-align:center; "><em> or    Call +60 327806938 to place an order</em></div>';
    }


    add_filter('woocommerce_get_availability', 'availability_filter_func');
    function availability_filter_func($availability)
    {
        $availability['availability'] = str_ireplace('Out of stock', 'Delivery Lead Time 4-8 Weeks', $availability['availability']);
        return $availability;
    }


    function backorder_text($availability)
    {
        foreach ($availability as $i) {

            $availability = str_replace('Available on backorder', 'This item will take 4-8 weeks to deliver', $availability);
        }

        return $availability;
    }
    add_filter('woocommerce_get_availability', 'backorder_text');
}

add_action('woocommerce_before_shop_loop_item', 'bbloomer_show_stock_shop', 1);

function bbloomer_show_stock_shop()
{
    global $product;
    if ($product->stock) { // if manage stock is enabled
        if (number_format($product->stock, 0, '', '') < 3) { // if stock is low
            echo '<div class="remaining"><span>Only ' . number_format($product->stock, 0, '', '') . ' left in stock!</span></div>';
        } else {
//            echo '<div class="remaining">' . number_format($product->stock,0,'','') . ' left in stock</div>';
        }
    }
}

require get_template_directory() . '/../schon-child/inc/template-tags.php';
