<?php
/**
 * Single Product tabs
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/single-product/tabs/tabs.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see 	https://docs.woothemes.com/document/template-structure/
 * @author  WooThemes
 * @package WooCommerce/Templates
 * @version 2.4.0
 */
global $product;
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

/**
 * Filter tabs and allow third parties to add their own.
 *
 * Each tab is an array containing title, callback and priority.
 * @see woocommerce_default_product_tabs()
 */
$tabs = apply_filters( 'woocommerce_product_tabs', array() );
if ( ! defined( 'ABSPATH' ) ) {
    exit; // Exit if accessed directly
}
function show_attr($key){
    global $product;
    if($product->get_attribute($key)){
        return $product->get_attribute($key);
    }else{
        return '-';
    }
}
global $post;

if ( ! $post->post_excerpt ) {
    return;
}
if ( ! empty( $tabs ) ) : ?>
<div class="product-detail">

    <div class="product-specification">
        <div class="row">
            <div class="col-sm-6 product-detail-item product-more-description">
                <?php echo call_user_func( $tabs['description']['callback'], $tabs['description']['title'], $tabs['description'] );  ?>

            </div>
            <div class="col-sm-6">
                <div class="col-sm-6 product-detail-item product-more-description">
                    <h2>Product Info</h2>
                <?php echo call_user_func( $tabs['additional_information']['callback'], $tabs['additional_information']['title'], $tabs['additional_information'] );  ?>
                </div>
                <div class="col-sm-6 product-detail-item product-more-description">
                    <h2>Product Dimension</h2>
                    <div class="product-attribute-item row">
                        <span class="__title col-xs-5">Dimension</span>
                        <span class="__value col-xs-7"><?php echo esc_html( wc_format_dimensions( $product->get_dimensions( false ) ) ); ?></span>
                    </div>
                    <div class="product-attribute-item row">
                        <span class="__title col-xs-5"><?php _e( 'Weight', 'woocommerce' ) ?></span>
                        <span class="__value col-xs-7"><?php echo esc_html( wc_format_weight( $product->get_weight() ) ); ?></span>
                    </div>
                    <div class="product-attribute-item row">
                        <span class="__title col-xs-5">Seating Depth</span>
                        <span class="__value col-xs-7"><?php echo show_attr('pa_seating_depth') ?></span>
                    </div>
                    <div class="product-attribute-item row">
                        <span class="__title col-xs-5">Seating Height<!-- -->:</span>
                        <span class="__value col-xs-7"><?php echo show_attr('pa_seating_height') ?></span>
                    </div>
                </div>
                <div class="col-sm-12 warranty-section product-more-description">
                    <?php echo call_user_func( $tabs['warranty-returns']['callback'], $tabs['warranty-returns']['title'], $tabs['warranty-returns'] );  ?>
                </div>
            </div>

        </div>
    </div>
    <div class="product-more-description">
<div class="row">
	<div class="col-sm-6 care product-detail-item">
			<?php echo call_user_func( $tabs['care']['callback'], $tabs['care']['title'], $tabs['care'] );  ?>
	</div>
			<div class="col-sm-6 reviews product-detail-item">
				<?php echo call_user_func( $tabs['reviews']['callback'], $tabs['reviews']['title'], $tabs['reviews'] );  ?>
			</div>
</div>
		</div>
    <div class="product-more-description">
        <?php
        unset($tabs['additional_information']);
        unset($tabs['description']);
        unset($tabs['warranty-returns']);
        unset($tabs['care']);
        unset($tabs['reviews']);
        foreach($tabs as $key => $tab):?>
        <div class="row">
            <div class="col-sm-12 <?php echo $key ?> product-detail-item">
            <?php echo call_user_func( $tab['callback'], $tab['title'], $tab );  ?>
            </div>
        </div>
        <?php endforeach; ?>

    </div>
</div>

<?php endif; ?>
