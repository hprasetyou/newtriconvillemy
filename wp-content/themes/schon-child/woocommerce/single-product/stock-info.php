<?php

    global $product;
    if ( $product->stock ) {
        if ( number_format( $product->stock,0,'','' ) > 0 ){
            ?>

            <div class="product-stock-info">
                <p style="font-size:1.1em"><i class="fa fa-building" ></i> This product is available in store</p>
            </div>

            <?php
        }
    }