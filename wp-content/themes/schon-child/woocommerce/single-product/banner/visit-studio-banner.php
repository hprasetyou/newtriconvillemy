<div class="row">
    <div class="col-sm-12 banner" style="height:400px; background:url('/wp-content/uploads/2017/10/Concrete-Patio-As-Walmart-Patio-Furniture-For-Awesome-Patio-Chair-Cover.jpg'); background-size: cover">
        <div class="col-sm-8 col-sm-offset-2">
            <h3>VISIT OUR STUDIO</h3>
            <p>Drop by and chat with our friendly crew or just relax with a coffee and browse our new collections. Some days tend to be busy, we suggest you book ahead if you can.</p>
            <a href="#" class="btn btn-default">Make an Appointment</a>
        </div>
    </div>
</div>