<?php
$proccess_time = 4 ;//weeks
$start_ready = date('M d', strtotime("+$proccess_time week"));
$end_ready = date('M d', strtotime("+$proccess_time+1 week"));


?>
<div>
    <div class="delivery-note-header">
        <div class="pull-right delivery-time">
            <span>Within <?php echo "$start_ready - $end_ready" ?> </span>
        </div>
        <h5 style="font-weight: 600;">Delivery in</h5>
    </div>
    <div class="delivery-note">
        <span><i class="fa fa-truck"></i> Free shipping and installation with purchase of RM 10K within Selangor </span>
    </div>
</div>
