<?php

/**
 * Product quantity inputs
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/global/quantity-input.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see     https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce/Templates
 * @version 3.4.0
 */

defined( 'ABSPATH' ) || exit;

if ( $max_value && $min_value === $max_value ) {
  ?>
	<div class="quantity hidden">
		<input type="hidden" id="<?php echo esc_attr( $input_id ); ?>" class="qty" name="<?php echo esc_attr( $input_name ); ?>" value="<?php echo esc_attr( $min_value ); ?>" />
	</div>
	<?php
} else {
  /* translators: %s: Quantity. */
  $labelledby = ! empty( $args['product_name'] ) ? sprintf( __( '%s quantity', 'woocommerce' ), strip_tags( $args['product_name'] ) ) : '';
  ?>
            <div class="input-group quantity-input">
                <span class="input-group-btn minus">
                    <button class="btn btn-default btn-dec" type="button"><i class="fa fa-minus"></i></button>
                </span>
                <input 
                    type="number"
			id="<?php echo esc_attr( $input_id ); ?>"
			step="<?php echo esc_attr( $step ); ?>"
			min="<?php echo esc_attr( $min_value ); ?>"
			max="<?php echo esc_attr( 0 < $max_value ? $max_value : '' ); ?>"
			name="<?php echo esc_attr( $input_name ); ?>"
			value="<?php echo esc_attr( $input_value ); ?>"
			title="<?php echo esc_attr_x( 'Qty', 'Product quantity input tooltip', 'woocommerce' ); ?>"
			pattern="<?php echo esc_attr( $pattern ); ?>"
			inputmode="<?php echo esc_attr( $inputmode ); ?>"
			aria-labelledby="<?php echo esc_attr( $labelledby ); ?>" 
                    class="form-control input-qty">
                <span class="input-group-btn plus">
                    <button class="btn btn-default btn-inc" type="button"><i class="fa fa-plus"></i></button>
                </span>
            </div><!-- /input-group -->
            <script>
                var $ = jQuery 
                jQuery('.quantity-input')
                .on('click','.btn-dec',function(){
                    jQuery('.input-qty').val(function(){
                        return jQuery(this).val()>1?jQuery(this).val()-1:jQuery(this).val()
                    })
                }).on('click','.btn-inc',function(){
                    jQuery('.input-qty').val(function(){
                        return (jQuery(this).val()*1)+1
                    })
                })
            </script>
	<?php
}