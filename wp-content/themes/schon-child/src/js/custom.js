$(window).on('load', function(){
var width = (window.innerWidth > 0) ? window.innerWidth : screen.width;
    if (width < 480 ){
        $('#wh-widget-send-button').appendTo('#primary-menu');
        $('#wh-widget-send-button').prop('style','width: 76px;height: 94px;position: absolute !important;padding-bottom: 10px !important;display: block');
    }

})
$( document).ready(function(){

  $('#main-section').find('.wpb_wrapper').first().not('.slick-initialized').slick({
    arrows:false,
    autoplay:true
    })
    $('.woocommerce-product-gallery__wrapper').before(function(){
      let gallery_nav = $('<div>',{class:'woocommerce-product-gallery__nav',html:$(this).html()});
      gallery_nav.children().each(function(i){
        $(this).find('img').appendTo($(this))
        $(this).find('a').remove()
      })
      return gallery_nav;
    })
    $('.woocommerce-product-gallery__wrapper').children().each(function(i){
      let prodimg =  $(this).find('a > img')
      prodimg.prop('src', function(){
        return $(this).prop('src').replace('100x100','600x400')})

      prodimg.prop('srcset',$(this).find('a').prop('href'))
    })
    $('.woocommerce-product-gallery__wrapper').slick({
      dots: false,
      arrows: false,
      adaptiveHeight: false
    })
    $('.woocommerce-product-gallery__nav').slick({
      slidesToShow: 5,
      slidesToScroll: 2,
      asNavFor: '.woocommerce-product-gallery__wrapper',
      dots: false,
      verticalSwiping: true,
      vertical:true,
      arrows: true,
      prevArrow: $('<div>',{html:$('<a>',{class:'top-arrow btn',html:$('<i>',{class:'fa fa-chevron-up'})})}).html(),
      nextArrow: $('<div>',{html:$('<a>',{class:'bottom-arrow btn',html:$('<i>',{class:'fa fa-chevron-down'})})}).html(),
      centerMode: true,
      focusOnSelect: true
    });
    $('.owl-products').slick({
      infinite: true,
      dots:false,
      slidesToShow: 5,
      slidesToScroll: 3
    })
    $('.accordion').click(function(){
        $(this).toggleClass('active')
    })
    $('.tc-product-grid .product').on('mouseover',function(){
        if($(this).parents('.products').hasClass('grid')){
            $(this).find('.woocommerce-product-bracket').show()
            $(this).find('.woocommerce-product-image-container').css('z-index',3);
        }
    }).on('mouseout',function(){
        if($(this).parents('.products').hasClass('grid')){
            $(this).find('.woocommerce-product-bracket').hide()
            $(this).find('.woocommerce-product-image-container').css('z-index',1);
        }
    })
    $('#list').click(function(){
        $('.woocommerce-product-bracket').hide()
        $('.woocommerce-product-image-container').css('z-index',1);
    })
    $('.cart-contents').click(function(e){
        e.preventDefault()
        $('.xoo-wsc-modal').toggleClass('xoo-wsc-active');
    })
var colright = $('<div/>',{class:'col-sm-3'})
var bs = $('.menu-item-has-children.depth-1').parents('.sub-menu-wrap')
colright.appendTo(bs)
$('.menu-item-has-children.depth-1').parents('.sub-menu').css('display','inherit').addClass('col-sm-8')
$('.menu-item-has-children.depth-1').eq(2).css('float','right')
$('.menu-item-has-children.depth-1').eq(3).css('float','right')
bs.addClass('submenu-container-bs')

$('.page-item-10117').prependTo('#pages-4 ul')
var width = (window.innerWidth > 0) ? window.innerWidth : screen.width;
    if (screen.width < 480 ){
        $('.site-title').find('img').css('width','100%');
        $('.site-title').find('img').css('height','auto');
        $('<div>', {id:"modalAddress",class:"modal fade",role:"dialog"}).appendTo('body');
        var md = $('<div>',{class:"modal-dialog"}).appendTo('#modalAddress');
        $('<div>', {class:"modal-content"}).appendTo(md)
        .append('<div class="modal-header"><button type="button" class="close" data-dismiss="modal">&times;</button><h4 class="modal-title">Visit Us</h4></div>')
        .append('<div class="modal-body"><div id="tc-address-detail"></div></div>');
        $('.tc-address-address-mobile').on('click','a',function(e){
            e.preventDefault();
            $('#modalAddress').modal('show');
            $('#tc-address-detail').html($('.tc-address-address').html());
        });
        $( window ).scroll(function() {
            if($('.site-title').find('img').css('height') != 'auto'){
                $('.site-title').find('img').css('height','auto');
                $('.site-title').find('img').css('width','100%');
            }
         $("#pwp-notification-button" ).css( "display", "none" ).fadeIn('fast');
         $("#wh-widget-send-button" ).css( "display", "none" ).fadeIn('fast');
        });
        $( "#pwp-notification-button" ).appendTo('#primary-menu');
        $( "#pwp-notification-button" ).css('position','absolute');
        $( "#pwp-notification-button" ).css('font-size','30px');

        // $('.wh-widget-right').appendTo('#primary-menu')
        $('.woocommerce-tabs').addClass('wc-accordion');
        $('.woocommerce-tabs').removeClass('wc-tabs-wrapper');
        $('.woocommerce-tabs').removeClass('woocommerce-tabs');
        $('.wc-tabs > li').each(function(i){
            var tab = $(this).find('a').attr('href');
            $(tab).appendTo($(this));
            $(this).find('a').data('target',tab);
            $(this).find('a').append('<span class="fa fa-plus pull-right"></span>');
            $(this).removeClass('active');
            console.log($(this));
            $(this).find('a').removeAttr('href');
            $(tab).hide();
        });
        $('.footer-widget-column').find('br').remove();
        $('.wc-tabs').on('click','a',function(e){
            e.preventDefault();
            $('.fa-minus').addClass('fa-plus');
            $('.fa-minus').removeClass('fa-minus');
            var plus = $(this).find('.fa-plus');
            var li = $(this).parents('li');
            plus.removeClass('fa-plus');
            $('.wc-tab').slideUp();
            if(! li.find('.wc-tab').is(":visible"))
            {
                $($(this).data('target')).slideDown('fast');
                plus.addClass('fa-minus');
            }else{
                li.removeClass('active');
                plus.addClass('fa-plus');
            }
        });
        if(!$('.quantity').is(':visible')){
            $('.qty-label').hide();
            $('.single_add_to_cart_button').css('margin-left','auto');
            $('.single_add_to_cart_button').css('margin-right','auto');
            $('.single_add_to_cart_button').css('display','block');
            $('.single_add_to_cart_button').css('width','80%');
        }else{
            $('.single_add_to_cart_button').width($('.form-group-add-to-cart').width()-($('.qty-label').width()+100+$('.quantity').width()));
        }
    }
});
