<nav id="tcv-main-nav" class="navbar navbar-default navbar-static-top">
    <div class="container">

        <div id="navbar" class="navbar-collapse collapse">
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <div class="navbar-brand site-brand navbar-collapse">
                           <?php schon_branding(); ?>
                    </div>
                        <div class="navbar-collapse" id="primary-navigation-menu-wrap">
		<?php
		if(has_nav_menu( 'primary' )) {
					//$navbar_align = (schon_get_option('primary-menu-align') == '2') ? 'navbar-right' : '';
		$navbar_align = 'pull-left';
		wp_nav_menu( array( 'theme_location' => 'primary', 'menu_id' => 'primary-menu', 'menu_class' => "nav navbar-nav $navbar_align", "container" => "", "walker" => new schonchild_Submenu_Wrap() ) );
		} else{
		echo "<p class=\"navbar-text pull-right\">".esc_html__('Register a primary navigation menu.', 'schon')."</p>";
                }
	    ?><div class="primary-menu-resposive-overlay"></div>
            </div>
           <?php
            $header_option = schon_get_sidebar_option();
            if( $header_option != 1): ?>
                <a id="sidebar-trigger-responsive" href="javascript:void(0)"><i class="icon icon-options-vertical"></i></a>
            <?php endif; ?>
            <ul class="nav navbar-nav navbar-right">
               <?php get_template_part('template-parts/header/icon-set'); ?>
            </ul>

        </div><!--/.nav-collapse -->

    </div>
</nav>
